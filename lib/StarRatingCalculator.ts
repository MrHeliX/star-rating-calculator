import { DifficultyCalculator } from "./DifficultyCalculator";
import { DifficultyHitObject } from "./Objects/osu/HitObjects/DifficultyHitObject";
import { OsuDifficultyHitObject } from "./Objects/osu/HitObjects/OsuDifficultyHitObject";
import { Aim } from "./Skills/Aim";
import { Flashlight } from "./Skills/Flashlight";
import { OsuStrainSkill } from "./Skills/OsuStrainSkill";
import { Skill } from "./Skills/Skill";
import { Speed } from "./Skills/Speed";

export interface SRCalculatorResponse {
    aim: number;
    aimNoSliders: number;
    speed: number;
    flashlight: number;
    total: number;
};

export class StarRatingCalculator extends DifficultyCalculator {
    private difficulty_multiplier: number = 0.0675;

    protected override CreateDifficultyAttributes(skills: Skill[]): SRCalculatorResponse {
        let aimRating = Math.sqrt(skills[0].DifficultyValue()) * this.difficulty_multiplier;
        let aimRatingNoSliders = Math.sqrt(skills[1].DifficultyValue()) * this.difficulty_multiplier;
        let speedRating = Math.sqrt(skills[2].DifficultyValue()) * this.difficulty_multiplier;

        let flashlightRating = 0;

        if (this.playableMods.includes("FL"))
            flashlightRating = Math.sqrt(skills[3].DifficultyValue()) * this.difficulty_multiplier;

        if (this.playableMods.includes("TD")) {
            aimRating = Math.pow(aimRating, 0.8);
            flashlightRating = Math.pow(flashlightRating, 0.8);
        }

        if (this.playableMods.includes("RX")) {
            aimRating *= 0.9;
            speedRating = 0;
            flashlightRating *= 0.7;
        }

        const baseAimPerformance = OsuStrainSkill.DifficultyToPerformance(aimRating);
        const baseSpeedPerformance = OsuStrainSkill.DifficultyToPerformance(speedRating);
        let baseFlashlightPerformance = 0;

        if (this.playableMods.includes("FL"))
            baseFlashlightPerformance = Flashlight.DifficultyToPerformance(flashlightRating);

        const basePerformance = Math.pow(
            Math.pow(baseAimPerformance, 1.1) + Math.pow(baseSpeedPerformance, 1.1) + Math.pow(baseFlashlightPerformance, 1.1),
            1 / 1.1
        );

        const starRating = basePerformance > 0.00001
            ? Math.cbrt(1.15) * 0.027 * (Math.cbrt(100000 / Math.pow(2, 1 / 1.1) * basePerformance) + 4)
            : 0;

        return { aim: aimRating, aimNoSliders: aimRatingNoSliders, speed: speedRating, flashlight: flashlightRating, total: starRating };
    };

    protected override CreateSkills(): Skill[] {
        const skills: Skill[] = [
            new Aim(this.playableMods, true),
            new Aim(this.playableMods, false),
            new Speed(this.playableMods)
        ];

        if (this.playableMods.includes("FL"))
            skills.push(new Flashlight(this.playableMods));

        return skills;
    };

    protected override CreateDifficultyHitObjects(): DifficultyHitObject[] {
        const objects: DifficultyHitObject[] = [];

        for (let i = 1; i < this.Beatmap.HitObjects.length; i++) {
            const lastLast = i > 1 ? this.Beatmap.HitObjects[i - 2] : null;
            objects.push(new OsuDifficultyHitObject(this.Beatmap.HitObjects[i], this.Beatmap.HitObjects[i - 1], this.clockRate, objects, objects.length, lastLast));
        }

        return objects;
    };
};

export default StarRatingCalculator;
