import { DifficultyCalculationUtils } from "../DifficultyCalculationUtils";
import { clamp } from "../helpers";
import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";
import { OsuDifficultyHitObject } from "../Objects/osu/HitObjects/OsuDifficultyHitObject";
import { Slider } from "../Objects/osu/HitObjects/Slider";
import { Spinner } from "../Objects/osu/HitObjects/Spinner";

export class AimEvaluator {
    public static NORMALISED_RADIUS = 50;
    public static NORMALISED_DIAMETER = this.NORMALISED_RADIUS * 2;

    private static wide_angle_multiplier = 1.5;
    private static acute_angle_multiplier = 1.95;
    private static slider_multiplier = 1.35;
    private static velocity_change_multiplier = 0.75;

    public static EvaluateDifficultyOf(current: DifficultyHitObject, withSliderTravelDistance: boolean): number {
        if (current.BaseObject instanceof Spinner || current.Index <= 1 || current.Previous(0).BaseObject instanceof Spinner)
            return 0;

        const osuCurrObj = current as OsuDifficultyHitObject;
        const osuLastObj = current.Previous(0) as OsuDifficultyHitObject;
        const osuLastLastObj = current.Previous(1) as OsuDifficultyHitObject;

        const radius = this.NORMALISED_RADIUS;
        const diameter = this.NORMALISED_DIAMETER;

        let currVelocity = osuCurrObj.LazyJumpDistance / osuCurrObj.StrainTime;

        if (osuLastObj.BaseObject instanceof Slider && withSliderTravelDistance) {
            const travelVelocity = osuLastObj.TravelDistance / osuLastObj.TravelTime;
            const movementVelocity = osuCurrObj.MinimumJumpDistance / osuCurrObj.MinimumJumpTime;

            currVelocity = Math.max(currVelocity, movementVelocity + travelVelocity);
        }

        let prevVelocity = osuLastObj.LazyJumpDistance / osuLastObj.StrainTime;

        if (osuLastLastObj.BaseObject instanceof Slider && withSliderTravelDistance) {
            const travelVelocity = osuLastLastObj.TravelDistance / osuLastLastObj.TravelTime;
            const movementVelocity = osuLastObj.MinimumJumpDistance / osuLastObj.MinimumJumpTime;

            prevVelocity = Math.max(prevVelocity, movementVelocity + travelVelocity);
        }

        let wideAngleBonus = 0;
        let acuteAngleBonus = 0;
        let sliderBonus = 0;
        let velocityChangeBonus = 0;

        let aimStrain = currVelocity;

        if (Math.max(osuCurrObj.StrainTime, osuLastObj.StrainTime) < 1.25 * Math.min(osuCurrObj.StrainTime, osuLastObj.StrainTime)) {
            if (osuCurrObj.Angle !== null && osuLastObj.Angle !== null && osuLastLastObj.Angle !== null) {
                const currAngle = osuCurrObj.Angle;
                const lastAngle = osuLastObj.Angle;
                const lastLastAngle = osuLastLastObj.Angle;

                const angleBonus = Math.min(currVelocity, prevVelocity);

                wideAngleBonus = this.calcWideAngleBonus(currAngle);
                acuteAngleBonus = this.calcAcuteAngleBonus(currAngle);

                if (DifficultyCalculationUtils.MillisecondsToBPM(osuCurrObj.StrainTime, 2) < 300)
                    acuteAngleBonus = 0;
                else {
                    acuteAngleBonus *= this.calcAcuteAngleBonus(lastAngle)
                        * Math.min(angleBonus, diameter * 1.25 / osuCurrObj.StrainTime)
                        * Math.pow(Math.sin(Math.PI / 2 * Math.min(1, (100 - osuCurrObj.StrainTime) / 25)), 2)
                        * Math.pow(Math.sin(Math.PI / 2 * (clamp(osuCurrObj.LazyJumpDistance, radius, diameter) - radius) / radius), 2);
                }

                wideAngleBonus *= angleBonus * (1 - Math.min(wideAngleBonus, Math.pow(this.calcWideAngleBonus(lastAngle), 3)));
                acuteAngleBonus *= 0.5 + 0.5 * (1 - Math.min(acuteAngleBonus, Math.pow(this.calcAcuteAngleBonus(lastLastAngle), 3)));
            }
        }

        if (Math.max(prevVelocity, currVelocity) !== 0) {
            prevVelocity = (osuLastObj.LazyJumpDistance + osuLastLastObj.TravelDistance) / osuLastObj.StrainTime;
            currVelocity = (osuCurrObj.LazyJumpDistance + osuLastObj.TravelDistance) / osuCurrObj.StrainTime;

            const distRatio = Math.pow(Math.sin(Math.PI / 2 * Math.abs(prevVelocity - currVelocity) / Math.max(prevVelocity, currVelocity)), 2);
            const overlapVelocityBuff = Math.min(diameter * 1.25 / Math.min(osuCurrObj.StrainTime, osuLastObj.StrainTime), Math.abs(prevVelocity - currVelocity));
            velocityChangeBonus = overlapVelocityBuff * distRatio;
            velocityChangeBonus *= Math.pow(Math.min(osuCurrObj.StrainTime, osuLastObj.StrainTime) / Math.max(osuCurrObj.StrainTime, osuLastObj.StrainTime), 2);
        }

        if (osuLastObj.BaseObject instanceof Slider)
            sliderBonus = osuLastObj.TravelDistance / osuLastObj.TravelTime;

        aimStrain += Math.max(acuteAngleBonus * this.acute_angle_multiplier, wideAngleBonus * this.wide_angle_multiplier + velocityChangeBonus * this.velocity_change_multiplier);

        if (withSliderTravelDistance)
            aimStrain += sliderBonus * this.slider_multiplier;

        return aimStrain;
    };

    private static calcWideAngleBonus(angle: number): number {
        return Math.pow(Math.sin(3 / 4 * (Math.min(5 / 6 * Math.PI, Math.max(Math.PI / 6, angle)) - Math.PI / 6)), 2);
    };

    private static calcAcuteAngleBonus(angle: number): number {
        return 1 - this.calcWideAngleBonus(angle);
    };
};