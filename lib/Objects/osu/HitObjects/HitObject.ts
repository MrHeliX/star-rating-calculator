import { round } from "lodash";
import { Beatmap } from "../Beatmap";
import { HitWindows } from "../HitWindows";

/**
 * Base class for all osu! hit objects
 */
export class HitObject {
    private control_point_leniency = 1;
    public StartTime = 0;
    public HitWindows: HitWindows;
    public NestedHitObjects: HitObject[] = [];

    constructor(hitObject: Partial<HitObject>) {
        this.StartTime = hitObject.StartTime;
    };

    public ApplyDefaults(beatmap: Beatmap): void {
        this.ApplyDefaultsToSelf(beatmap);
        this.NestedHitObjects.length = 0;

        this.CreateNestedHitObjects();

        this.NestedHitObjects.sort((a, b) => a.StartTime - b.StartTime);

        for (const h of this.NestedHitObjects) {
            h.ApplyDefaults(beatmap);
        }
    };

    protected ApplyDefaultsToSelf(beatmap: Beatmap): void {
        this.HitWindows = this.CreateHitWindows();
        this.HitWindows?.setDifficulty(beatmap.Difficulty.OverallDifficulty);
    };

    public GetEndTime(): number {
        return round((this as any).EndTime ?? this.StartTime, 0);
    };

    protected CreateHitWindows(): HitWindows {
        return new HitWindows();
    };

    protected CreateNestedHitObjects(): void {

    };

    protected AddNested(hitObject: HitObject): void {
        this.NestedHitObjects.push(hitObject);
    };
};