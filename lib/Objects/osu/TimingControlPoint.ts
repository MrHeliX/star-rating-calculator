import { ControlPoint, ControlPointType } from "./ControlPoint";

export class TimingControlPoint extends ControlPoint {
    public readonly TimeSignature: number;
    public static readonly DEFAULT: TimingControlPoint = new TimingControlPoint({});
    
    public DEFAULT_BEAT_LENGTH = 1000;
    public BeatLength = 0
    public get BPM() { return 60000 / this.BeatLength; };

    constructor(timingControlPoint: Partial<TimingControlPoint>) {
        super();
        this.Time = timingControlPoint.Time;
        this.BeatLength = timingControlPoint.BeatLength ?? this.DEFAULT_BEAT_LENGTH;
    };

    public override GetType(): ControlPointType {
        return ControlPointType.TIMING;
    };
};
