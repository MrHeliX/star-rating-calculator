import { Beatmap } from "./Objects/osu/Beatmap";
import { ControlPoint } from "./Objects/osu/ControlPoint";
import { DifficultyControlPoint } from "./Objects/osu/DifficultyControlPoint";
import { Slider } from "./Objects/osu/HitObjects/Slider";
import { TimingControlPoint } from "./Objects/osu/TimingControlPoint";

export const clamp = (value: number, min: number, max: number): number => Math.min(Math.max(value, min), max);
export const lerp = (x, y, a) => x * (1 - a) + y * a;

enum EqualitySelection {
    FirstFound = 0,
    Leftmost = 1,
    Rightmost = 2
};

export const BinarySearchTimingPoint = <T extends TimingControlPoint | DifficultyControlPoint>(list: T[], time: number) => {
    let index = BinarySearch(list, time, EqualitySelection.Rightmost);
    if (index < 0)
        index = ~index - 1;

    return index >= 0 ? list[index] : null;
};

export const BinarySearch = (list: ControlPoint[], time: number, equalitySelection: EqualitySelection): number => {
    const n = list.length;

    if (n === 0)
        return -1;

    if (time < list[0].Time)
        return -1;

    if (time > list[list.length - 1].Time)
        return ~n;

    let l = 0;
    let r = n - 1;
    let equalityFound = false;

    while (l <= r) {
        const pivot = l + ((r - l) >> 1);

        if (list[pivot].Time < time)
            l = pivot + 1;
        else if (list[pivot].Time > time)
            r = pivot - 1;
        else {
            equalityFound = true;
            switch (equalitySelection) {
                case EqualitySelection.FirstFound:
                    r = pivot - 1;
                    break;
                case EqualitySelection.Leftmost:
                    r = pivot - 1;
                    break;
                case EqualitySelection.Rightmost:
                default:
                    l = pivot + 1;
                    break;
            };
        }
    }

    if (!equalityFound)
        return ~l;

    switch (equalitySelection) {
        case EqualitySelection.Leftmost:
            return l;
        case EqualitySelection.Rightmost:
        default:
            return l - 1;
    };
};

export const GetPrecisionAdjustedBeatLength = (slider: Slider, timingPoint: Beatmap["TimingPoints"][0], rulesetShortName: string): number => {
    const sliderVelocityAsBeatLength = -100 / slider.SliderVelocityMultiplier;
    let bpmMultiplier: number;

    switch (rulesetShortName) {
        case "taiko": case "mania":
            bpmMultiplier = sliderVelocityAsBeatLength < 0 ? clamp(-sliderVelocityAsBeatLength, 10, 10000) / 100 : 1;
            break;
        case "osu": case "fruits":
            bpmMultiplier = sliderVelocityAsBeatLength < 0 ? clamp(-sliderVelocityAsBeatLength, 10, 1000) / 100 : 1;
            break;
    };

    return timingPoint.BeatLength * bpmMultiplier;
};
