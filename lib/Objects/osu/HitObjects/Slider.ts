import { Vector2 } from "../../Vector2";
import { SliderTailCircle } from "./SliderObjects/SliderTailCircle";
import { SliderTick } from "./SliderObjects/SliderTick";
import { OsuHitObject } from "./OsuHitObject";
import { SliderHeadCircle } from "./SliderObjects/SliderHeadCircle";
import { Beatmap } from "../Beatmap";
import { BinarySearchTimingPoint, GetPrecisionAdjustedBeatLength } from "../../../helpers";
import { SliderEventGenerator, SliderEventType } from "../../../SliderEventGenerator";
import { SliderPath } from "../../../SliderPath";
import { PathControlPoint } from "../../../PathControlPoint";
import { SliderRepeat } from "./SliderObjects/SliderRepeat";
import { TypedCache } from "../../Cache";
import { HitWindows } from "../HitWindows";
import { EmptyHitWindows } from "./EmptyHitWindows";

export class Slider extends OsuHitObject {
    public get EndTime() { return this.StartTime + this.SpanCount() * this.Path.Distance / this.Velocity; };
    public get Duration() { return this.EndTime - this.StartTime; };

    public readonly endPositionCache = new TypedCache<Vector2>();
    public override get EndPosition() {
        if (this.endPositionCache.IsValid)
            return this.endPositionCache.Value
        return this.endPositionCache.Value = this.Position.add(this.CurvePositionAt(1));
    };
    public StackedPositionAt(t: number) {
        return this.StackedPosition.add(this.CurvePositionAt(t));
    };

    private readonly path: SliderPath = new SliderPath({ ControlPoints: [], OptimiseCatmull: true });
    public get Path() { return this.path; };
    public set Path(value: SliderPath) {
        if (!value) return;
        this.path.ControlPoints.length = 0;
        this.path.ControlPoints.push(...value.ControlPoints.map(c => new PathControlPoint(c.Position, c.Type)));
        this.path.ExpectedDistance = value.ExpectedDistance;
    };

    public get Distance() { return this.Path.Distance; };
    public override get Position() { return super.Position; }
    public override set Position(value: Vector2) {
        super.Position = value;
        this.updateNestedPositions();
    };

    public LazyEndPosition?: Vector2;
    public LazyTravelDistance: number = 0;
    public LazyTravelTime: number = 0;
    private repeatCount: number = 0;
    public get RepeatCount() { return this.repeatCount; };
    public set RepeatCount(value: number) {
        this.repeatCount = value;
        this.updateNestedPositions();
    };

    public get SpanDuration() { return this.Duration / this.SpanCount(); };
    public Velocity: number = 1;
    public TickDistance: number = 0;
    public TickDistanceMultiplier: number = 1;

    private classicSliderBehaviour: boolean;
    public get ClassicSliderBehaviour() { return this.classicSliderBehaviour; };
    public set ClassicSliderBehaviour(value: boolean) {
        this.classicSliderBehaviour = value;
        if (!!this.HeadCircle)
            this.HeadCircle.ClassicSliderBehaviour = value;
        if (!!this.TailCircle)
            this.TailCircle.ClassicSliderBehaviour = value;
    };

    public SliderVelocityMultiplier: number = 1;

    public HeadCircle: SliderHeadCircle;
    public TailCircle: SliderTailCircle;
    public LastRepeat: SliderRepeat;

    constructor(slider: Partial<Slider>) {
        super(slider);
        this.Path = slider.Path;
    };

    public SpanCount(): number {
        return this.RepeatCount + 1;
    };

    public CurvePositionAt(progress: number): Vector2 {
        return this.Path.PositionAt(this.ProgressAt(progress));
    };

    public ProgressAt(progress: number): number {
        let p = progress * this.SpanCount() % 1;
        if (this.SpanAt(progress) % 2 === 1)
            p = 1 - p;
        return p;
    };

    public SpanAt(progress: number): number {
        return progress * this.SpanCount();
    };

    protected override ApplyDefaultsToSelf(beatmap: Beatmap): void {
        super.ApplyDefaultsToSelf(beatmap);

        const timingPoint = BinarySearchTimingPoint(beatmap.TimingPoints, this.StartTime) ?? beatmap.TimingPoints[0];
        this.Velocity = this.BASE_SCORING_DISTANCE * beatmap.Difficulty.SliderMultiplier / GetPrecisionAdjustedBeatLength(this, timingPoint, "osu");
        const scoringDistance = this.Velocity * timingPoint.BeatLength;
        this.TickDistance = scoringDistance / beatmap.Difficulty.SliderTickRate * this.TickDistanceMultiplier;
    };

    protected override CreateNestedHitObjects(): void {
        super.CreateNestedHitObjects();
        
        const sliderEvents = SliderEventGenerator.Generate(this.StartTime, this.SpanDuration, this.Velocity, this.TickDistance, this.Path.Distance, this.SpanCount());

        for (const e of sliderEvents) {
            switch (e.Type) {
                case SliderEventType.Tick:
                    this.AddNested(new SliderTick({
                        SpanIndex: e.SpanIndex,
                        SpanStartTime: e.SpanStartTime,
                        StartTime: e.Time,
                        Position: this.Position.add(this.Path.PositionAt(e.PathProgress)),
                        PathProgress: e.PathProgress,
                        StackHeight: this.StackHeight
                    }));
                    break;
                case SliderEventType.Head:
                    this.HeadCircle = new SliderHeadCircle({
                        StartTime: e.Time,
                        Position: this.Position,
                        StackHeight: this.StackHeight,
                        ClassicSliderBehaviour: this.ClassicSliderBehaviour
                    });
                    this.AddNested(this.HeadCircle);
                    break;
                case SliderEventType.Tail:
                    this.TailCircle = new SliderTailCircle({
                        ...this,
                        RepeatIndex: e.SpanIndex,
                        StartTime: e.Time,
                        Position: this.EndPosition,
                        StackHeight: this.StackHeight,
                        ClassicSliderBehaviour: this.ClassicSliderBehaviour,
                        Slider: this
                    });
                    this.AddNested(this.TailCircle);
                    break;
                case SliderEventType.Repeat:
                    this.LastRepeat = new SliderRepeat({
                        ...this,
                        RepeatIndex: e.SpanIndex,
                        StartTime: this.StartTime + (e.SpanIndex + 1) * this.SpanDuration,
                        Position: this.Position.add(this.Path.PositionAt(e.PathProgress)),
                        StackHeight: this.StackHeight,
                        PathProgress: e.PathProgress,
                        Slider: this
                    });
                    this.AddNested(this.LastRepeat);
                    break;
            }
        }
    };

    public updateNestedPositions(): void {
        this.endPositionCache.Invalidate();

        for (const nested of this.NestedHitObjects) {
            if (nested instanceof SliderHeadCircle)
                nested.Position = this.Position;
            else if (nested instanceof SliderTailCircle)
                nested.Position = this.EndPosition;
            else if (nested instanceof SliderRepeat)
                nested.Position = this.Position.add(this.Path.PositionAt(nested.PathProgress));
            else if (nested instanceof SliderTick)
                nested.Position = this.Position.add(this.Path.PositionAt(nested.PathProgress));
        }
    };

    protected override CreateHitWindows(): HitWindows {
        return new EmptyHitWindows();
    };
};