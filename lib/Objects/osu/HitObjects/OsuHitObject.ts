import { Vector2 } from "../../Vector2";
import { Beatmap } from "../Beatmap";
import { GetDifficultyRange, GetDifficultyRangeSingle, HitWindows } from "../HitWindows";
import { HitObject } from "./HitObject";
import { OsuHitWindows } from "./OsuHitWindows";

export class OsuHitObject extends HitObject {
    public OBJECT_RADIUS = 64;
    public readonly OBJECT_DIMENSIONS = new Vector2(this.OBJECT_RADIUS * 2, this.OBJECT_RADIUS * 2);
    public BASE_SCORING_DISTANCE = 100;
    public PREEMPT_MIN = 450;
    public PREEMPT_MID = 1200;
    public PREEMPT_MAX = 1800;

    public TimePreempt = 600;
    public TimeFadeIn = 400;

    private position: Vector2;
    public get Position() { return this.position; };
    public set Position(value: Vector2) {
        this.position = value;
    };

    public get StackedPosition() { return this.Position.add(this.StackOffset); };
    public get EndPosition() { return this.Position; };
    public get StackedEndPosition() { return this.EndPosition.add(this.StackOffset); };
    public StackHeight = 0;
    public get StackOffset() { return new Vector2(this.StackHeight * this.Scale * -6.4); };
    public get Radius() { return this.OBJECT_RADIUS * this.Scale; };
    public Scale = 0;

    constructor(osuHitObject: Partial<OsuHitObject>) {
        super(osuHitObject);

        if (osuHitObject.Position) this.Position = osuHitObject.Position;
        if (osuHitObject.StackHeight) this.StackHeight = osuHitObject.StackHeight;
        if (osuHitObject.Scale) this.Scale = osuHitObject.Scale;
    };

    protected override ApplyDefaultsToSelf(beatmap: Beatmap): void {
        super.ApplyDefaultsToSelf(beatmap);
        this.TimePreempt = GetDifficultyRange(beatmap.Difficulty.ApproachRate, this.PREEMPT_MAX, this.PREEMPT_MID, this.PREEMPT_MIN);
        this.TimeFadeIn = 400 * Math.min(1, this.TimePreempt / this.PREEMPT_MIN);
        this.Scale = CalculateScaleFromCircleSize(beatmap.Difficulty.CircleSize, true);
    };

    protected override CreateHitWindows(): HitWindows {
        return new OsuHitWindows();
    };
};

const CalculateScaleFromCircleSize = (circleSize: number, applyFudge: boolean = false): number => {
    const broken_gamefield_rounding_allowance = 1.00041;
    return (1 - 0.7 * GetDifficultyRangeSingle(circleSize)) / 2 * (applyFudge ? broken_gamefield_rounding_allowance : 1);
};
