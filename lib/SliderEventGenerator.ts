import { clamp } from "./helpers";

export class SliderEventGenerator {
    public static TAIL_LENIENCY = -36;

    public static Generate(startTime: number, spanDuration: number, velocity: number, tickDistance: number, totalDistance: number, spanCount: number): SliderEventDescriptor[] {
        const max_length = 100000;

        const length = Math.min(max_length, totalDistance);
        tickDistance = clamp(tickDistance, 0, length);
        const minDistanceFromEnd = velocity * 10;

        const result: SliderEventDescriptor[] = [];

        result.push(new SliderEventDescriptor(SliderEventType.Head, startTime, 0, startTime, 0));

        if (tickDistance !== 0) {
            for (let span = 0; span < spanCount; span++) {
                const spanStartTime = startTime + span * spanDuration;
                const reversed = span % 2 === 1;

                const ticks = this.generateTicks(span, spanStartTime, spanDuration, reversed, length, tickDistance, minDistanceFromEnd);

                if (reversed)
                    ticks.reverse();

                result.push(...ticks);

                if (span < spanCount - 1)
                    result.push(new SliderEventDescriptor(SliderEventType.Repeat, spanStartTime + spanDuration, span, startTime + span * spanDuration, (span + 1) % 2));
            }
        }

        const totalDuration = spanCount * spanDuration;
        const finalSpanIndex = spanCount - 1;
        const finalSpanStartTime = startTime + finalSpanIndex * spanDuration;
        const legacyLastTickTime = Math.max(startTime + totalDuration / 2, finalSpanStartTime + spanDuration + this.TAIL_LENIENCY);
        let legacyLastTickProgress = (legacyLastTickTime - finalSpanStartTime) / spanDuration;

        if (spanCount % 2 === 0)
            legacyLastTickProgress = 1 - legacyLastTickProgress;

        result.push(new SliderEventDescriptor(SliderEventType.LegacyLastTick, legacyLastTickTime, finalSpanIndex, finalSpanStartTime, legacyLastTickProgress));
        result.push(new SliderEventDescriptor(SliderEventType.Tail, startTime + totalDuration, finalSpanIndex, startTime + (spanCount - 1) * spanDuration, spanCount % 2));
        return result;
    };

    private static generateTicks(spanIndex: number, spanStartTime: number, spanDuration: number, reversed: boolean, length: number, tickDistance: number, minDistanceFromEnd: number): SliderEventDescriptor[] {
        const result: SliderEventDescriptor[] = [];
        for (let d = tickDistance; d <= length; d += tickDistance) {
            if (d >= length - minDistanceFromEnd)
                break;

            const pathProgress = d / length;
            const timeProgress = reversed ? 1 - pathProgress : pathProgress;

            result.push(new SliderEventDescriptor(SliderEventType.Tick, spanStartTime + timeProgress * spanDuration, spanIndex, spanStartTime, pathProgress));
        }

        return result;
    };
};

export enum SliderEventType {
    Tick = 0,
    LegacyLastTick = 1,
    Head = 2,
    Tail = 3,
    Repeat = 4
};

export class SliderEventDescriptor {
    public Type: SliderEventType;
    public Time = 0;
    public SpanIndex = 0;
    public SpanStartTime = 0;
    public PathProgress = 0;

    constructor(type: SliderEventType, time: number, spanIndex: number, spanStartTime: number, pathProgress: number) {
        this.Type = type;
        this.Time = time;
        this.SpanIndex = spanIndex;
        this.SpanStartTime = spanStartTime;
        this.PathProgress = pathProgress;
    };
};
