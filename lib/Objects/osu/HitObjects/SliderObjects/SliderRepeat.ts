import { SliderEndCircle } from "./SliderEndCircle";

export class SliderRepeat extends SliderEndCircle {
    public PathProgress = 0;

    constructor(sliderRepeat: Partial<SliderRepeat>) {
        super(sliderRepeat);
        this.PathProgress = sliderRepeat.PathProgress;
    };
};
