import { OsuService } from "./osu-service";
import BeatmapParser from "./BeatmapParser";
import StarRatingCalculator, { SRCalculatorResponse } from "./StarRatingCalculator";
import { Beatmap as OsuBeatmap } from "./Objects/osu/Beatmap";
import { InputOptions, Output, SRValues, StarRatingModCombination } from "./Interfaces/interfaces";

const osuService = new OsuService();
let beatmapParser;
let Beatmap;
let mods: string[] = [];

/**
 * Function to calculate star ratings for a given beatmap
 * @param {number} map_id The beatmap id for which to calculate the star rating
 * @param {InputOptions} [options] Optional parameter with options
 * @returns {Promise<Output>} The star ratings
 * @example
 * // Nomod star rating
 * const response = await calculateStarRating(1616712);
 * 
 * // HDDT star rating
 * const response = await calculateStarRating(1616712, { mods: ["HD", "DT"] });
 * 
 * // All possible star ratings
 * const response = await calculateStarRating(1616712, { allModCombinations: true });
 */
export async function calculateStarRating(map_id: number, options?: InputOptions): Promise<Output> {
    beatmapParser = new BeatmapParser();
    Beatmap = null;

    const map = !!options?.path
        ? await osuService.getOsuBeatmapFromFile(map_id, options.path, options.checksum)
        : await osuService.getOsuBeatmap(map_id);

    if (map === null)
        throw new Error("No map found for specified map id");

    if (options?.mods)
        mods = parseMods(options.mods);

    Beatmap = beatmapParser.parseBeatmap(map, mods);

    if (!options?.allModCombinations) {
        const label = (mods.length > 0 ? mods.join('') : "NM") as StarRatingModCombination;
        const response = calculateNextModCombination(map, mods, true);

        const srValues: SRValues = {
            star_rating: response.total,
            aim_rating: response.aim,
            speed_rating: response.speed,
            flashlight_rating: response.flashlight
        };

        const output: Output = {
            map_id,
            star_ratings: {
                [label]: srValues
            }
        };

        return output;
    }
    else {
        const allModCombinations = getAllModCombinations();
        const output: Output = {
            map_id,
            star_ratings: allModCombinations.reduce((total, current) => {
                const label = current.mods.length > 0 ? current.mods.join('') : "NM";
                const response = calculateNextModCombination(map, current.mods, current.reParse);
        
                const srValues: SRValues = {
                    star_rating: response.total,
                    aim_rating: response.aim,
                    speed_rating: response.speed,
                    flashlight_rating: response.flashlight 
                };
        
                total[label] = srValues;
                return total;
            }, {})
        };

        return output;
    }
}

const parseMods = (_mods: string[]) => {
    if (!_mods.length) return [];

    const srMods = ["EZ", "HR", "HT", "DT", "FL"];
    return _mods.filter(m => srMods.includes(m)).sort((a, b) => srMods.indexOf(a) - srMods.indexOf(b));
};

const calculateNextModCombination = (map: string, mods: string[], reParse: boolean): SRCalculatorResponse => {
    let beatmap: OsuBeatmap = reParse
        ? beatmapParser.parseBeatmap(map, mods)
        : { ...Beatmap };

    Beatmap = beatmap;

    if (mods.includes("HD"))
        beatmapParser.ApplyHidden(beatmap);

    const timeRate = getTimeRate(mods);
    const starRatingCalculator = new StarRatingCalculator(beatmap, mods, timeRate);
    return starRatingCalculator.Calculate();
};

const getTimeRate = (mods: string[]): number => {
    if (mods.includes("DT"))
        return 1.5;
    if (mods.includes("HT"))
        return 0.75;
    return 1;
};

const getAllModCombinations = (): Array<{ mods: string[], reParse?: boolean }> => {
    return [
        { mods: [], reParse: true },
        { mods: ["DT"] },
        { mods: ["HT"] },
        { mods: ["FL"] },
        // { mods: ["HD", "FL"] },

        { mods: ["DT", "FL"] },
        { mods: ["HT", "FL"] },
        // { mods: ["HD", "DT", "FL"] },
        // { mods: ["HD", "HT", "FL"] },

        { mods: ["HR"], reParse: true },
        { mods: ["HR", "DT"] },
        { mods: ["HR", "HT"] },
        { mods: ["HR", "FL" ] },
        { mods: ["HR", "DT", "FL"] },
        { mods: ["HR", "HT", "FL"] },
        // { mods: ["HD", "HR", "FL"] },
        // { mods: ["HD", "HR", "DT", "FL"] },
        // { mods: ["HD", "HR", "HT", "FL"] },

        { mods: ["EZ"], reParse: true },
        { mods: ["EZ", "DT"] },
        { mods: ["EZ", "HT"] },
        { mods: ["EZ", "FL"] },
        // { mods: ["EZ", "HD", "FL"] },
        { mods: ["EZ", "DT", "FL"] },
        { mods: ["EZ", "HT", "FL"] },
        // { mods: ["EZ", "HD", "DT", "FL"] },
        // { mods: ["EZ", "HD", "HT", "FL"] }
    ];
};
