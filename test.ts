import { calculateStarRating } from "./lib";

const calculate = async () => {
    const id = 53;

    const res = await calculateStarRating(id, {
        path: `${__dirname}/test/osu_files/${id}.osu`,
        allModCombinations: true,
        checksum: "fd7172b241772846a0839dc935325405"
    });

    // const res2 = await calculateStarRating(id, {
    //     returnAllValues: true
    // });

    console.log(res);
    // console.log(res2);
};

calculate();