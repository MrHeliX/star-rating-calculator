import { PathType } from "./Objects/osu/PathType";
import { Vector2 } from "./Objects/Vector2";

export class PathControlPoint {
    private position: Vector2;
    public get Position() { return this.position; };
    public set Position(value: Vector2) {
        if (!!this.position && value.equals(this.position)) return;
        this.position = value;
    };

    private type?: PathType;
    public get Type() { return this.type; };
    public set Type(value: PathType) {
        if (value === this.type) return;
        this.type = value;
    };

    constructor(position: Vector2, type: PathType = null) {
        this.Position = position;
        this.Type = type;
    };

    public Equals(other: PathControlPoint): boolean {
        return this.Position.equals(other.Position) && this.Type === other.Type;
    };

    public ToString(): string {
        return !this.type
            ? `Position=${this.Position.x},${this.Position.y}`
            : `Position=${this.Position.x},${this.Position.y} Type=${this.Type}`;
    };
};
