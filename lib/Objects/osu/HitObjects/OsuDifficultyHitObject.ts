import { clamp } from "../../../helpers";
import { Vector2 } from "../../Vector2";
import { HitResult } from "../HitResult";
import { DifficultyHitObject } from "./DifficultyHitObject";
import { HitObject } from "./HitObject";
import { OsuHitObject } from "./OsuHitObject";
import { Slider } from "./Slider";
import { SliderRepeat } from "./SliderObjects/SliderRepeat";
import { SliderTick } from "./SliderObjects/SliderTick";
import { Spinner } from "./Spinner";

export const NORMALISED_RADIUS = 50;
export const NORMALISED_DIAMETER = NORMALISED_RADIUS * 2;
export const MIN_DELTA_TIME = 25;

export class OsuDifficultyHitObject extends DifficultyHitObject {
    private maximum_slider_radius = NORMALISED_RADIUS * 2.4;
    private assumed_slider_radius = NORMALISED_RADIUS * 1.8;

    public BaseObject: OsuHitObject;
    public readonly StrainTime: number;
    public LazyJumpDistance = 0;
    public MinimumJumpDistance = 0;
    public MinimumJumpTime = 0;
    public TravelDistance = 0;
    public TravelTime = 0;
    public Angle = null;
    public HitWindowGreat = 0;
    
    private readonly lastLastObject: OsuHitObject;
    private readonly lastObject: OsuHitObject;

    constructor(hitObject: HitObject, lastObject: HitObject, clockRate: number, objects: DifficultyHitObject[], index: number, lastLastObject?: HitObject) {
        super(hitObject, lastObject, clockRate, objects, index);

        this.lastLastObject = lastLastObject as OsuHitObject;
        this.lastObject = lastObject as OsuHitObject;

        this.StrainTime = Math.max(this.DeltaTime, MIN_DELTA_TIME);

        if (this.BaseObject instanceof Slider) {
            const sliderObject = this.BaseObject as Slider;
            this.HitWindowGreat = 2 * sliderObject.HeadCircle.HitWindows.WindowFor(HitResult.Great) / clockRate;
        }
        else
            this.HitWindowGreat = 2 * this.BaseObject.HitWindows.WindowFor(HitResult.Great) / clockRate;

        this.setDistances(clockRate);
    };

    private setDistances(clockRate: number): void {
        if (this.BaseObject instanceof Slider) {
            const currentSlider = this.BaseObject as Slider;
            this.computeSliderCursorPosition(currentSlider);
            
            this.TravelDistance = currentSlider.LazyTravelDistance * Math.pow(1 + currentSlider.RepeatCount / 2.5, 1 / 2.5);
            this.TravelTime = Math.max(currentSlider.LazyTravelTime / clockRate, MIN_DELTA_TIME);
        }

        if (this.BaseObject instanceof Spinner || this.lastObject instanceof Spinner)
            return;

        let scalingFactor = NORMALISED_RADIUS / this.BaseObject.Radius;

        if (this.BaseObject.Radius < 30) {
            const smallCircleBonus = Math.min(30 - this.BaseObject.Radius, 5) / 50;
            scalingFactor *= 1 + smallCircleBonus;
        }

        const lastCursorPosition: Vector2 = this.getEndCursorPosition(this.lastObject);

        this.LazyJumpDistance = (this.BaseObject.StackedPosition.scale(scalingFactor).subtract(lastCursorPosition.scale(scalingFactor))).length();
        this.MinimumJumpTime = this.StrainTime;
        this.MinimumJumpDistance = this.LazyJumpDistance;

        if (this.lastObject instanceof Slider) {
            const lastSlider = this.lastObject as Slider;
            const lastTravelTime = Math.max(lastSlider.LazyTravelTime / clockRate, MIN_DELTA_TIME);
            this.MinimumJumpTime = Math.max(this.StrainTime - lastTravelTime, MIN_DELTA_TIME);

            const tailJumpDistance = lastSlider.TailCircle.StackedPosition.subtract(this.BaseObject.StackedPosition).length() * scalingFactor;
            this.MinimumJumpDistance = Math.max(0, Math.min(this.LazyJumpDistance - (this.maximum_slider_radius - this.assumed_slider_radius), tailJumpDistance - this.maximum_slider_radius));
        }

        if (!!this.lastLastObject && !(this.lastLastObject instanceof Spinner)) {
            const lastLastCursorPosition: Vector2 = this.getEndCursorPosition(this.lastLastObject);

            const v1: Vector2 = lastLastCursorPosition.subtract(this.lastObject.StackedPosition);
            const v2: Vector2 = this.BaseObject.StackedPosition.subtract(lastCursorPosition);

            let dot = v1.dot(v2);
            let det = v1.x * v2.y - v1.y * v2.x;

            this.Angle = Math.abs(Math.atan2(det, dot));
        }
    };

    private computeSliderCursorPosition(slider: Slider): void {
        if (!!slider.LazyEndPosition)
            return;

        let trackingEndTime = Math.max(
            slider.StartTime + slider.Duration - 36,
            slider.StartTime + slider.Duration / 2
        );

        let nestedHitObjects: HitObject[] = slider.NestedHitObjects;
        let lastRealTick: SliderTick = null;

        for (const hitObject of slider.NestedHitObjects) {
            if (hitObject instanceof SliderTick)
                lastRealTick = hitObject;
        }

        if (lastRealTick?.StartTime > trackingEndTime) {
            trackingEndTime = lastRealTick.StartTime;

            const reordered: HitObject[] = nestedHitObjects.filter(o => o.StartTime !== lastRealTick.StartTime);
            reordered.push(lastRealTick);
            nestedHitObjects = reordered;
        }

        slider.LazyTravelTime = trackingEndTime - slider.StartTime;

        let endTimeMin = slider.LazyTravelTime / slider.SpanDuration;
        if (endTimeMin % 2 >= 1)
            endTimeMin = 1 - endTimeMin % 1;
        else
            endTimeMin = endTimeMin % 1;

        slider.LazyEndPosition = slider.StackedPosition.add(slider.Path.PositionAt(endTimeMin));

        let currCursorPosition: Vector2 = slider.StackedPosition;
        const scalingFactor = NORMALISED_RADIUS / slider.Radius;

        for (let i = 1; i < nestedHitObjects.length; i++) {
            const currMovementObj = nestedHitObjects[i] as OsuHitObject;

            let currMovement: Vector2 = currMovementObj.StackedPosition.subtract(currCursorPosition);
            let currMovementLength = scalingFactor * currMovement.length();

            let requiredMovement = this.assumed_slider_radius;

            if (i === nestedHitObjects.length - 1) {
                const lazyMovement: Vector2 = slider.LazyEndPosition.subtract(currCursorPosition);
                if (lazyMovement.length() < currMovement.length())
                    currMovement = lazyMovement;

                currMovementLength = scalingFactor * currMovement.length();
            }
            else if (currMovementObj instanceof SliderRepeat)
                requiredMovement = NORMALISED_RADIUS;

            if (currMovementLength > requiredMovement) {
                currCursorPosition = currCursorPosition.add(currMovement.scale((currMovementLength - requiredMovement) / currMovementLength));
                currMovementLength *= (currMovementLength - requiredMovement) / currMovementLength;
                slider.LazyTravelDistance += currMovementLength;
            }

            if (i === nestedHitObjects.length - 1)
                slider.LazyEndPosition = currCursorPosition;
        }
    }

    private getEndCursorPosition(hitObject: OsuHitObject): Vector2 {
        let pos: Vector2 = hitObject.StackedPosition;

        if (hitObject instanceof Slider) {
            const slider = hitObject as Slider;
            this.computeSliderCursorPosition(slider);
            pos = slider.LazyEndPosition ?? pos;
        }

        return pos;
    };

    public GetDoubleTapness(osuNextObj?: OsuDifficultyHitObject): number {
        if (!osuNextObj) return 0;

        const currDeltaTime = Math.max(1, this.DeltaTime);
        const nextDeltaTime = Math.max(1, osuNextObj.DeltaTime);
        const deltaDifference = Math.abs(nextDeltaTime - currDeltaTime);
        const speedRatio = currDeltaTime / Math.max(currDeltaTime, deltaDifference);
        const windowRatio = Math.pow(Math.min(1, currDeltaTime / this.HitWindowGreat), 2);
        return 1 - Math.pow(speedRatio, 1 - windowRatio);
    };

    public OpacityAt(time: number, hidden: boolean): number {
        if (time > this.BaseObject.StartTime)
            return 0;

        const fadeInStartTime = this.BaseObject.StartTime - this.BaseObject.TimePreempt;
        const fadeInDuration = this.BaseObject.TimeFadeIn;

        if (hidden) {
            const fadeOutStartTime = this.BaseObject.StartTime - this.BaseObject.TimePreempt + this.BaseObject.TimeFadeIn;
            const fadeOutDuration = this.BaseObject.TimePreempt * 0.3;

            return Math.min(
                clamp((time - fadeInStartTime) / fadeInDuration, 0, 1),
                1 - clamp((time - fadeOutStartTime) / fadeOutDuration, 0, 1)
            );
        }

        return clamp((time - fadeInStartTime) / fadeInDuration, 0, 1);
    };
};
