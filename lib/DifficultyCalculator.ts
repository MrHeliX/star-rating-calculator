import { Beatmap } from "./Objects/osu/Beatmap";
import { DifficultyHitObject } from "./Objects/osu/HitObjects/DifficultyHitObject";
import { Skill } from "./Skills/Skill";
import { SRCalculatorResponse } from "./StarRatingCalculator";

export abstract class DifficultyCalculator {
    protected Beatmap: Beatmap;
    protected playableMods: string[];
    protected clockRate = 0;

    constructor(beatmap: Beatmap, mods: string[], clockRate: number) {
        this.Beatmap = beatmap;
        this.playableMods = mods;
        this.clockRate = clockRate;
    };

    public Calculate() {
        const skills = this.CreateSkills();

        if (!this.Beatmap.HitObjects.length)
            return this.CreateDifficultyAttributes(skills);

        const difficultyHitObjects = this.CreateDifficultyHitObjects();
        for (const hitObject of difficultyHitObjects) {
            for (const skill of skills) {
                skill.Process(hitObject);
            }
        }

        return this.CreateDifficultyAttributes(skills);
    };

    protected abstract CreateDifficultyAttributes(skills: Skill[]): SRCalculatorResponse;

    protected abstract CreateSkills(): Skill[];

    protected abstract CreateDifficultyHitObjects(): DifficultyHitObject[];
};
