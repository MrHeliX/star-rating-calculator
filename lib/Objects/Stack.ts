export class Stack<T> {
    private _array: T[];
    private _size = 0;
    private _version = 0;

    private DefaultCapacity = 4;

    constructor(collection?: T[]) {
        if (!collection)
            this._array = [];
        else
            this._array = [...collection];
    };

    public get Count() { return this._size; };

    public Clear(): void {
        this._array.length = 0;
        this._size = 0;
        this._version++;
    };

    public Contains(item: T): boolean {
        return this._size !== 0 && this._array.lastIndexOf(item, this._size - 1) !== -1;
    };

    public Pop(): T {
        const size = this._size - 1;
        const array: T[] = this._array;

        this._version++;
        this._size = size;
        const item: T = array[size];
        return item;
    };

    public Push(item: T): void {
        const size = this._size;
        const array: T[] = this._array;

        array[size] = item;
        this._version++;
        this._size = size + 1;
    };
};
