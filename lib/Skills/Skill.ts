import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";

export abstract class Skill {
    protected Mods: string[];

    constructor(mods: string[]) {
        this.Mods = mods;
    };

    public abstract Process(current: DifficultyHitObject): void;

    public abstract DifficultyValue(): number;
};
