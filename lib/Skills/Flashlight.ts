import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";
import { FlashlightEvaluator } from "./FlashlightEvaluator";
import { StrainSkill } from "./StrainSkill";

export class Flashlight extends StrainSkill {
    private hasHiddenMod: boolean;

    constructor(mods: string[]) {
        super(mods);
        this.hasHiddenMod = mods.includes("HD");
    };

    private skillMultiplier = 0.05512;
    private strainDecayBase = 0.15;
    private currentStrain = 0;

    private strainDecay(ms: number) {
        return Math.pow(this.strainDecayBase, ms / 1000);
    };

    protected override CalculateInitialStrain(time: number, current: DifficultyHitObject): number {
        return this.currentStrain * this.strainDecay(time - current.Previous(0).StartTime);
    };

    protected override StrainValueAt(current: DifficultyHitObject): number {
        this.currentStrain *= this.strainDecay(current.DeltaTime);
        this.currentStrain += FlashlightEvaluator.EvaluateDifficultyOf(current, this.hasHiddenMod) * this.skillMultiplier;

        return this.currentStrain;
    };

    public override DifficultyValue(): number {
        return this.GetCurrentStrainPeaks().reduce((a, b) => a + b, 0);
    };

    public static DifficultyToPerformance(difficulty: number): number {
        return 25 * Math.pow(difficulty, 2);
    };
};
