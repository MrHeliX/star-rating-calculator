export interface InputOptions {
    /** A path to the .osu file. If this is omitted, or if the file is not found, then the file will be downloaded from osu directly */
    path?: string,

    /** Only used if the `path` parameter is included. This checksum will be compared to the checksum of the file found and if they don't match, a new version will be downloaded from osu */
    checksum?: string,
    
    /** An array with mod acronym (HD, DT, HR, etc) to apply before calculating star rating */
    mods?: string[],

    /** If you set this to true, all unique star ratings for all possible mod combinations will be calculated. The `mods` parameter will be ignored */
    allModCombinations?: boolean
};

export type StarRatingModCombination =
    "NM" |
    "EZ" |
    "HR" |
    "HT" |
    "DT" |
    "FL" |
    "EZHT" |
    "EZDT" |
    "EZFL" |
    "EZHTFL" |
    "EZDTFL" |
    "HRHT" |
    "HRDT" |
    "HRFL" |
    "HRHTFL" |
    "HRDTFL" |
    "HTFL" |
    "DTFL";

export interface SRValues {
    star_rating: number,
    aim_rating?: number,
    speed_rating?: number,
    flashlight_rating?: number
};

export interface Output {
    map_id: number,
    star_ratings: Partial<Record<StarRatingModCombination, SRValues>>
};
