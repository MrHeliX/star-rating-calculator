import { HitResult } from "../HitResult";
import { DifficultyRange, HitWindows } from "../HitWindows";

export class OsuHitWindows extends HitWindows {
    public MISS_WINDOW = 400;

    private readonly OSU_RANGES: DifficultyRange[] = [
        new DifficultyRange(HitResult.Great, 80, 50, 20),
        new DifficultyRange(HitResult.OK, 140, 100, 60),
        new DifficultyRange(HitResult.Meh, 200, 150, 100),
        new DifficultyRange(HitResult.Miss, this.MISS_WINDOW, this.MISS_WINDOW, this.MISS_WINDOW)
    ];

    public override IsHitResultAllowed(result: HitResult): boolean {
        switch (result) {
            case HitResult.Great:
            case HitResult.OK:
            case HitResult.Meh:
            case HitResult.Miss:
                return true;
        }

        return false;
    };

    protected override GetRanges(): DifficultyRange[] {
        return this.OSU_RANGES;
    };
};
