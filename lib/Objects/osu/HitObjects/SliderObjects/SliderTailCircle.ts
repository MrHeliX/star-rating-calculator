import { SliderEndCircle } from "./SliderEndCircle";

export class SliderTailCircle extends SliderEndCircle {
    public ClassicSliderBehaviour: boolean;

    constructor(sliderTailCircle: Partial<SliderTailCircle>) {
        super(sliderTailCircle);
        this.ClassicSliderBehaviour = sliderTailCircle.ClassicSliderBehaviour;
    };
};
