import { OsuHitObject } from "./OsuHitObject";

export class Spinner extends OsuHitObject {
    public get EndTime() { return this.StartTime + this.Duration; };
    public set EndTime(value: number) {
        this.Duration = value - this.StartTime;
    };

    public Duration = 0;

    constructor(spinner: Partial<Spinner>) {
        super(spinner);
        this.Duration = spinner.Duration;
    };
};
