export enum SplineType {
    Catmull,
    Bezier,
    BSpline,
    Linear,
    PerfectCurve
};

export class PathType {
    public Type: SplineType;
    public Degree? = null;

    constructor(splineType: SplineType, degree: number = null) {
        this.Type = splineType;
        this.Degree = null;
    };

    public static readonly CATMULL: PathType = new PathType(SplineType.Catmull);
    public static readonly BEZIER: PathType = new PathType(SplineType.Bezier);
    public static readonly LINEAR: PathType = new PathType(SplineType.Linear);
    public static readonly PERFECT_CURVE: PathType = new PathType(SplineType.PerfectCurve);

    public static BSpline(degree: number): PathType {
        return new PathType(SplineType.BSpline, degree);
    };
};
