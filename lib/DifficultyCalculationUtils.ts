export class DifficultyCalculationUtils {
    public static BPMToMilliseconds(bpm: number, delimiter: number = 4): number {
        return 60000 / delimiter / bpm;
    };

    public static MillisecondsToBPM(ms: number, delimiter: number = 4): number {
        return 60000 / (ms * delimiter);
    };

    public static Logistic(x: number, midpointOffset: number, multiplier: number, maxValue: number = 1) {
        return maxValue / (1 + Math.exp(multiplier * (midpointOffset - x)));
    };
};
