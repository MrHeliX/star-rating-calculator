import axios from "axios";
import fs from "fs";
import { createHash } from "crypto";

export class OsuService {
    async getOsuBeatmap(map_id: number): Promise<string> {
        const osuResponse = await axios.get(`https://osu.ppy.sh/osu/${map_id}`);
        if (osuResponse.status === 200) {
            return osuResponse.data;
        }
        else
            throw new Error(osuResponse.data);
    };

    async getOsuBeatmapFromFile(map_id: number, path: string, checksum): Promise<string> {
        try {
            if (!!checksum) {
                const md5 = await createMD5(path);
                if (!md5 || md5 !== checksum)
                    return await this.getOsuBeatmap(map_id);
            }

            const fileRes = fs.readFileSync(path).toString();

            return fileRes;
        } catch (error) {
            return await this.getOsuBeatmap(map_id);
        };
    };
};

const createMD5 = (filepath: string): Promise<string | null> => {
    return new Promise<string>(resolve => {
        try {
            const hash = createHash("md5");

            const rStream = fs.createReadStream(filepath);
            rStream.on("data", data => {
                hash.update(data);
            });

            rStream.on("end", () => {
                resolve(hash.digest("hex"));
            });

            rStream.on("error", () => {
                resolve(null);
            });
        } catch (error) {
            resolve(null);
        };
    });
};
