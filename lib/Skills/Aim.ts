import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";
import { OsuStrainSkill } from "./OsuStrainSkill";
import { AimEvaluator } from "./AimEvaluator";

export class Aim extends OsuStrainSkill {
    private readonly withSliders: boolean;
    private currentStrain = 0;
    private skillMultiplier = 25.18;
    private strainDecayBase = 0.15;

    constructor(mods: string[], withSliders: boolean) {
        super(mods);
        this.withSliders = withSliders;
    };

    private strainDecay(ms: number): number {
        return Math.pow(this.strainDecayBase, ms / 1000);
    };

    protected override CalculateInitialStrain(time: number, current: DifficultyHitObject): number {
        return this.currentStrain * this.strainDecay(time - current.Previous(0).StartTime);
    };

    protected override StrainValueAt(current: DifficultyHitObject): number {
        this.currentStrain *= this.strainDecay(current.DeltaTime);
        this.currentStrain += AimEvaluator.EvaluateDifficultyOf(current, this.withSliders) * this.skillMultiplier;

        return this.currentStrain;
    };
};
