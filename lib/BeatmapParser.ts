import { HitType } from "./Objects/osu/HitType";
import { PathType } from "./Objects/osu/PathType";
import { Vector2 } from "./Objects/Vector2";
import { Beatmap } from "./Objects/osu/Beatmap";
import { HitObject } from "./Objects/osu/HitObjects/HitObject";
import { HitCircle } from "./Objects/osu/HitObjects/HitCircle";
import { Slider } from "./Objects/osu/HitObjects/Slider";
import { Spinner } from "./Objects/osu/HitObjects/Spinner";
import { Precision } from "./Precision";
import { SliderPath } from "./SliderPath";
import { PathControlPoint } from "./PathControlPoint";
import { TimingControlPoint } from "./Objects/osu/TimingControlPoint";
import { DifficultyControlPoint } from "./Objects/osu/DifficultyControlPoint";
import { BinarySearchTimingPoint } from "./helpers";
import { OsuHitObject } from "./Objects/osu/HitObjects/OsuHitObject";

const MAX_COORDINATE_VALUE = 131072;

class BeatmapParser {
    private beatmap: Beatmap;
    private stack_distance = 3;
    private offset = 0;

    private parseDifficultyValue(key: string, value: string, mods: string[]): void {
        let parsedValue = parseFloat(value);

        if (mods.includes("HR")) {
            switch (key) {
                case "CircleSize":
                    parsedValue = Math.min(parsedValue * 1.3, 10);
                    break;
                case "HPDrainRate": case "OverallDifficulty": case "ApproachRate":
                    parsedValue = Math.min(parsedValue * 1.4, 10);
                    break;
            };
        }

        if (mods.includes("EZ") && ["CircleSize", "HPDrainRate", "OverallDifficulty", "ApproachRate"].includes(key))
            parsedValue *= 0.5;

        this.beatmap.Difficulty[key] = parsedValue;

        if (key === "OverallDifficulty" && this.beatmap.Difficulty.ApproachRate === undefined)
            this.beatmap.Difficulty.ApproachRate = parsedValue;
    };

    parseBeatmap(data: string, mods: string[]): Beatmap {
        if (!data) throw new Error('No beatmap found');

        this.beatmap = new Beatmap();

        let section = null;
        let lines = data.split('\n').map(line => line.trim());
        for (let line of lines) {
            if (line.startsWith('//')) continue
            if (!line) continue;
            if (!section && line.includes('osu file format v')) {
                this.beatmap.Version = parseInt(line.split('osu file format v')[1], 10);
                continue;
            }
            if (/^\s*\[(.+?)\]\s*$/.test(line)) {
                section = /^\s*\[(.+?)\]\s*$/.exec(line)[1];
                continue;
            }

            if (this.beatmap.Version < 5)
                this.offset = 24;

            switch (section) {
                case 'General': {
                    let [key, value] = line.split(':').map(v => v.trim())
                    if (key === 'StackLeniency')
                        this.beatmap.StackLeniency = parseFloat(value);
                    break;
                }
                case 'Difficulty': {
                    let [key, value] = line.split(':').map(v => v.trim());
                    this.parseDifficultyValue(key, value, mods);
                    break;
                }
                case 'TimingPoints': {
                    let split = line.split(',');

                    const time = +split[0] + this.offset;
                    const beatLength = +split[1];
                    const speedMultiplier = beatLength < 0 || isNaN(beatLength) ? 100 / -beatLength : 1;
                    let timeSignature: number = 4;
                    if (split.length >= 3)
                        timeSignature = split[2][0] === '0' ? 4 : +split[2];

                    let timingChange: boolean = true;
                    if (split.length >= 7)
                        timingChange = split[6][0] === '1';

                    if (timingChange) {
                        if (isNaN(beatLength))
                            throw new Error("Beatlength cannot be NaN in a timing control point");

                        const controlPoint = new TimingControlPoint({
                            Time: time,
                            BeatLength: beatLength,
                            TimeSignature: timeSignature
                        });

                        this.beatmap.TimingPoints.push(controlPoint);
                    }

                    const controlPoint = new DifficultyControlPoint({
                        Time: time,
                        SliderVelocity: speedMultiplier,
                        GenerateTicks: !isNaN(beatLength)
                    });

                    this.beatmap.DifficultyTimingPoints.push(controlPoint);
                    break;
                }
                case 'HitObjects': {
                    let split = line.split(',');
                    const pos = new Vector2(+split[0], +split[1]);
                    const startTime = +split[2] + this.offset;
                    const hitType = +split[3];

                    let result: HitObject = null;

                    if (hitType & HitType.Normal)
                        result = this.createCircle(pos);

                    if (hitType & HitType.Slider) {
                        let length = null;
                        let repeatCount = +split[6];
                        if (repeatCount > 9000) throw new Error("Repeat count is to high");

                        repeatCount = Math.max(0, repeatCount - 1);

                        if (split.length > 7) {
                            length = Math.max(0, Math.min(+split[7], MAX_COORDINATE_VALUE));
                            if (length === 0)
                                length = null;
                        }

                        let pathType: PathType;
                        const pointSplit = split[5].split("|");

                        let points = [new PathControlPoint(new Vector2(0))];
                        pointSplit.forEach(point => {
                            if (point.length === 1) {
                                pathType = this.getPathType(point);
                                points[0].Type = pathType;
                                return;
                            }

                            const temp = point.split(":");
                            points.push(new PathControlPoint(new Vector2(+temp[0], +temp[1]).subtract(pos)));
                        });

                        const points2: PathControlPoint[] = [];
                        points.forEach((p, i) => {
                            if (i > 0 && i !== points.length - 1 && p.Position.equals(points[i - 1].Position)) {
                                points[i - 1].Type = pathType;
                                return;
                            }

                            points2.push(p);
                        });

                        const isLinear = (p0: Vector2, p1: Vector2, p2: Vector2): boolean => {
                            return Precision.almostEqualsNumber(0, (p1.y - p0.y) * (p2.x - p0.x) - (p1.x - p0.x) * (p2.y - p0.y));
                        };

                        if (points2.length === 3 && pathType === PathType.PERFECT_CURVE && isLinear(points2[0].Position, points2[1].Position, points2[2].Position))
                            pathType = PathType.LINEAR;

                        result = this.createSlider(pos, points2, length, repeatCount);
                    }

                    if (hitType & HitType.Spinner) {
                        const duration = Math.max(0, +split[5] + this.offset - startTime);

                        result = this.createSpinner(new Vector2(512, 384).divide(2), duration);
                    }

                    result.StartTime = startTime;
                    this.beatmap.HitObjects.push(result);
                }
            }
        }

        this.beatmap.HitObjects.sort((a, b) => a.StartTime - b.StartTime);

        this.beatmap.HitObjects.forEach(hitObject => {
            this.applyDefaults(hitObject);
        });

        if (mods.includes("HR")) {
            this.beatmap.HitObjects.forEach(hitObject => {
                if (hitObject instanceof OsuHitObject) {
                    hitObject.Position = new Vector2(hitObject.Position.x, 384 - hitObject.Position.y);

                    if (hitObject instanceof Slider) {
                        hitObject.Path.MirrorPath();
                        hitObject.endPositionCache.Invalidate();
                        hitObject.updateNestedPositions();
                    }
                }
            });
        }

        this.ApplyStacking();

        return this.beatmap;
    };

    public ApplyHidden(beatmap: Beatmap): void {
        beatmap.HitObjects.forEach(hitObject => {
            if (hitObject instanceof OsuHitObject)
                this.applyHidden(hitObject);
        });
    };

    private applyHidden(hitObject: OsuHitObject): void {
        hitObject.TimeFadeIn = hitObject.TimePreempt * 0.4;
        hitObject.NestedHitObjects.forEach(nested => {
            if (nested instanceof OsuHitObject)
                this.applyHidden(nested);
        });
    };

    private createCircle(pos: Vector2): HitCircle {
        return new HitCircle({ Position: pos });
    };

    private createSlider(pos: Vector2, controlPoints: PathControlPoint[], length: number, repeatCount: number): Slider {
        const slider = new Slider({});
        slider.Position = pos;
        slider.Path = new SliderPath({ ControlPoints: controlPoints, ExpectedDistance: length });
        slider.RepeatCount = repeatCount;
        return slider;
    };

    private createSpinner(pos: Vector2, duration: number): Spinner {
        return new Spinner({ Position: pos, Duration: duration });
    };

    private applyDefaults(hitObject: HitObject): void {
        if (hitObject instanceof Slider) {
            const difficultyControlPoint = BinarySearchTimingPoint(this.beatmap.DifficultyTimingPoints, hitObject.StartTime) ?? DifficultyControlPoint.DEFAULT;
            hitObject.SliderVelocityMultiplier = difficultyControlPoint.SliderVelocity;
        }

        hitObject.ApplyDefaults(this.beatmap);
    };

    private ApplyStacking(): void {
        if (!this.beatmap.HitObjects.length) return;

        const hitObjects = this.beatmap.HitObjects as OsuHitObject[];

        hitObjects.forEach(h => {
            h.StackHeight = 0;
        });

        if (this.beatmap.Version >= 6)
            this.applyStackingNew(hitObjects, 0, hitObjects.length - 1);
        else
            this.applyStackingOld(hitObjects);
    };

    private applyStackingNew(hitObjects: OsuHitObject[], startIndex: number, endIndex: number): void {
        let extendedEndIndex = endIndex;

        if (endIndex < hitObjects.length - 1) {
            for (let i = endIndex; i >= startIndex; i--) {
                let stackBaseIndex = i;

                for (let n = stackBaseIndex + 1; n < hitObjects.length; n++) {
                    const stackBaseObject = hitObjects[stackBaseIndex];
                    if (stackBaseObject instanceof Spinner) break;

                    const objectN = hitObjects[n];
                    if (objectN instanceof Spinner) continue;

                    const endTime = stackBaseObject.GetEndTime();
                    const stackThreshold = objectN.TimePreempt * this.beatmap.StackLeniency;

                    if (objectN.StartTime - endTime > stackThreshold)
                        break;

                    if (stackBaseObject.Position.distance(objectN.Position) < this.stack_distance || (stackBaseObject instanceof Slider && stackBaseObject.EndPosition.distance(objectN.Position) < this.stack_distance)) {
                        stackBaseIndex = n;
                        objectN.StackHeight = 0;
                    }
                }

                if (stackBaseIndex > extendedEndIndex) {
                    extendedEndIndex = stackBaseIndex;
                    if (extendedEndIndex === hitObjects.length - 1)
                        break;
                }
            }
        }

        let extendedStartIndex = startIndex;

        for (let i = extendedEndIndex; i > startIndex; i--) {
            let n = i;

            let objectI = hitObjects[i];
            if (objectI.StackHeight !== 0 || objectI instanceof Spinner) continue;

            const stackThreshold = objectI.TimePreempt * this.beatmap.StackLeniency;

            if (objectI instanceof HitCircle) {
                while (--n >= 0) {
                    const objectN = hitObjects[n];
                    if (objectN instanceof Spinner) continue;

                    const endTime = objectN.GetEndTime();

                    if (objectI.StartTime - endTime > stackThreshold)
                        break;

                    if (n < extendedStartIndex) {
                        objectN.StackHeight = 0;
                        extendedStartIndex = n;
                    }

                    if (objectN instanceof Slider && objectN.EndPosition.distance(objectI.Position) < this.stack_distance) {
                        const offset = objectI.StackHeight - objectN.StackHeight + 1;

                        for (let j = n + 1; j <= i; j++) {
                            const objectJ = hitObjects[j];
                            if (objectN.EndPosition.distance(objectJ.Position) < this.stack_distance)
                                objectJ.StackHeight -= offset;
                        }

                        break;
                    }

                    if (objectN.Position.distance(objectI.Position) < this.stack_distance) {
                        objectN.StackHeight = objectI.StackHeight + 1;
                        objectI = objectN;
                    }
                }
            }
            else if (objectI instanceof Slider) {
                while (--n >= startIndex) {
                    const objectN = hitObjects[n];
                    if (objectN instanceof Spinner) continue;

                    if (objectI.StartTime - objectN.StartTime > stackThreshold)
                        break;

                    if (objectN.EndPosition.distance(objectI.Position) < this.stack_distance) {
                        objectN.StackHeight = objectI.StackHeight + 1;
                        objectI = objectN;
                    }
                }
            }
        }
    };

    private applyStackingOld(hitObjects: OsuHitObject[]): void {
        for (let i = 0; i < hitObjects.length; i++) {
            const currHitObject = hitObjects[i];

            if (currHitObject.StackHeight !== 0 && !(currHitObject instanceof Slider))
                continue;

            let startTime = currHitObject.GetEndTime();
            let sliderStack = 0;

            for (let j = i + 1; j < hitObjects.length; j++) {
                const stackThreshold = hitObjects[i].TimePreempt * this.beatmap.StackLeniency;

                if (hitObjects[j].StartTime - stackThreshold > startTime)
                    break;

                const position2 = currHitObject instanceof Slider
                    ? currHitObject.Position.add(currHitObject.Path.PositionAt(1))
                    : currHitObject.Position;

                if (hitObjects[j].Position.distance(currHitObject.Position) < this.stack_distance) {
                    currHitObject.StackHeight++;
                    startTime = hitObjects[j].StartTime;
                }
                else if (hitObjects[j].Position.distance(position2) < this.stack_distance) {
                    sliderStack++;
                    hitObjects[j].StackHeight -= sliderStack;
                    startTime = hitObjects[j].StartTime;
                }
            }
        }
    };

    private getPathType(input: string): PathType {
        switch (input) {
            default:
            case "C":
                return PathType.CATMULL;
            case "B":
                return PathType.BEZIER;
            case "L":
                return PathType.LINEAR;
            case "P":
                return PathType.PERFECT_CURVE;
        };
    };
};

export default BeatmapParser;
