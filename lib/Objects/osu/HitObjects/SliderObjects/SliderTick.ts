import { OsuHitObject } from "../OsuHitObject";
import { Beatmap } from "../../Beatmap";
import { HitWindows } from "../../HitWindows";
import { EmptyHitWindows } from "../EmptyHitWindows";

export class SliderTick extends OsuHitObject {
    public SpanIndex: number = 0;
    public SpanStartTime: number = 0;
    public PathProgress: number = 0;

    constructor(sliderTick: Partial<SliderTick>) {
        super(sliderTick);
        this.SpanIndex = sliderTick.SpanIndex;
        this.SpanStartTime = sliderTick.SpanStartTime;
        this.PathProgress = sliderTick.PathProgress;
    };

    protected override ApplyDefaultsToSelf(beatmap: Beatmap): void {
        super.ApplyDefaultsToSelf(beatmap);

        let offset: number;

        if (this.SpanIndex > 0)
            offset = 200;
        else
            offset = this.TimePreempt * 0.66;

        this.TimePreempt = (this.StartTime - this.SpanStartTime) / 2 + offset;
    };

    protected override CreateHitWindows(): HitWindows {
        return new EmptyHitWindows();
    };
};