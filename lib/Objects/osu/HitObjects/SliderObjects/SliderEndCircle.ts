import { Beatmap } from "../../Beatmap";
import { HitWindows } from "../../HitWindows";
import { EmptyHitWindows } from "../EmptyHitWindows";
import { HitCircle } from "../HitCircle";
import { Slider } from "../Slider";

export class SliderEndCircle extends HitCircle {
    public readonly Slider: Slider;
    public RepeatIndex = 0;
    public get SpanDuration() { return this.Slider.SpanDuration; };
    
    constructor(sliderEndCircle: Partial<SliderEndCircle>) {
        super(sliderEndCircle);
        this.Slider = sliderEndCircle.Slider;
        this.RepeatIndex = sliderEndCircle.RepeatIndex;
    };

    protected override ApplyDefaultsToSelf(beatmap: Beatmap): void {
        super.ApplyDefaultsToSelf(beatmap);

        if (this.RepeatIndex > 0) {
            this.TimeFadeIn = 0;
            this.TimePreempt = this.SpanDuration * 2;
        }
        else
            this.TimePreempt += this.StartTime - this.Slider.StartTime;
    };

    protected override CreateHitWindows(): HitWindows {
        return new EmptyHitWindows();
    };
};
