export class TypedCache<T> {
    private value: T;
    public get Value() {
        if (!this.IsValid)
            throw new Error("Invalid cache");
        return this.value;
    };
    public set Value(value: T) {
        this.value = value;
        this.IsValid = true;
    };

    public IsValid: boolean;

    public Invalidate(): boolean {
        if (this.IsValid) {
            this.IsValid = false;
            return true;
        }

        return false;
    };
};

export class Cache {
    public IsValid: boolean;

    public Invalidate(): boolean {
        if (this.IsValid) {
            this.IsValid = false;
            return true;
        }

        return false;
    };

    public Validate(): void {
        this.IsValid = true;
    };
}
