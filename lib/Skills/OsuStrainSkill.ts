import { clamp, lerp } from "../helpers";
import { StrainSkill } from "./StrainSkill";

export abstract class OsuStrainSkill extends StrainSkill {
    public ReducedSectionCount: number = 10;
    public ReducedStrainBaseline = 0.75;

    public DifficultyValue(): number {
        let difficulty = 0;
        let weight = 1;

        const peaks = this.GetCurrentStrainPeaks().filter(p => p > 0);
        const strains = peaks.sort((a, b) => b - a);

        for (let i = 0; i < Math.min(strains.length, this.ReducedSectionCount); i++) {
            const scale = Math.log10(lerp(1, 10, clamp(i / this.ReducedSectionCount, 0, 1)));
            strains[i] *= lerp(this.ReducedStrainBaseline, 1, scale);
        }

        for (const strain of strains.sort((a, b) => b - a)) {
            difficulty += strain * weight;
            weight *= this.DecayWeight;
        }

        return difficulty;
    };

    public static DifficultyToPerformance(difficulty: number): number {
        return Math.pow(5 * Math.max(1, difficulty / 0.0675) - 4, 3) / 100000;
    };
};
