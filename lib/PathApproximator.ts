import { CircularArcProperties } from "./CircularArcProperties";
import { Stack } from "./Objects/Stack";
import { Vector2 } from "./Objects/Vector2";

export class PathApproximator {
    public static BEZIER_TOLERANCE: number = 0.25;
    private static catmull_detail: number = 50;
    private static circular_arc_tolerance: number = 0.1;

    public static BezierToPiecewiseLinear(controlPoints: Vector2[]): Vector2[] {
        return this.BSplineToPiecewiseLinear(controlPoints, Math.max(1, controlPoints.length - 1));
    };

    public static BSplineToPiecewiseLinear(controlPoints: Vector2[], degree: number): Vector2[] {
        if (controlPoints.length < 2)
            return controlPoints.length === 0 ? [] : [controlPoints[0]];

        degree = Math.min(degree, controlPoints.length - 1);

        const output: Vector2[] = [];
        const pointCount = controlPoints.length - 1;

        const toFlatten: Stack<Vector2[]> = this.bSplineToBezierInternal(controlPoints, degree);
        const freeBuffers: Stack<Vector2[]> = new Stack();

        const subdivisionBuffer1: Vector2[] = [];
        const subdivisionBuffer2: Vector2[] = [];

        const leftChild: Vector2[] = subdivisionBuffer2;

        while (toFlatten.Count > 0) {
            const parent = toFlatten.Pop();
            if (this.bezierIsFlatEnough(parent)) {
                this.bezierApproximate(parent, output, subdivisionBuffer1, subdivisionBuffer2, degree + 1);

                freeBuffers.Push(parent);
                continue;
            }

            const rightChild = freeBuffers.Count > 0 ? freeBuffers.Pop() : [];
            this.bezierSubdivide(parent, leftChild, rightChild, subdivisionBuffer1, degree + 1);

            for (let i = 0; i < degree + 1; i++) {
                parent[i] = leftChild[i];
            }

            toFlatten.Push(rightChild);
            toFlatten.Push(parent);
        }

        output.push(controlPoints[pointCount]);
        return output;
    };

    public static CatmullToPiecewiseLinear(controlPoints: Vector2[]): Vector2[] {
        const result: Vector2[] = [];

        for (let i = 0; i < controlPoints.length - 1; i++) {
            const v1 = i > 0 ? controlPoints[i - 1] : controlPoints[i];
            const v2 = controlPoints[i];
            const v3 = i < controlPoints.length - 1 ? controlPoints[i + 1] : v2.add(v2).subtract(v1);
            const v4 = i < controlPoints.length - 2 ? controlPoints[i + 2]: v3.add(v3).subtract(v2);

            for (let c = 0; c < this.catmull_detail; c++) {
                result.push(this.catmullFindPoint(v1, v2, v3, v4, c / this.catmull_detail));
                result.push(this.catmullFindPoint(v1, v2, v3, v4, (c + 1) / this.catmull_detail));
            }
        }

        return result;
    };

    public static CircularArcToPiecewiseLinear(controlPoints: Vector2[]): Vector2[] {
        const pr: CircularArcProperties = new CircularArcProperties(undefined, controlPoints);
        if (!pr.IsValid)
            return this.BezierToPiecewiseLinear(controlPoints);

        const amountPoints = 2 * pr.Radius <= this.circular_arc_tolerance ? 2 : Math.max(2, Math.ceil(pr.ThetaRange / (2 * Math.acos(1 - this.circular_arc_tolerance / pr.Radius))));
        const output: Vector2[] = [];

        for (let i = 0; i < amountPoints; i++) {
            const fract = i / (amountPoints - 1);
            const theta = pr.ThetaStart + pr.Direction * fract * pr.ThetaRange;
            const o = new Vector2(Math.cos(theta), Math.sin(theta)).scale(pr.Radius);
            output.push(pr.Centre.add(o));
        }

        return output;
    };

    public static LinearToPiecewiseLinear(controlPoints: Vector2[]): Vector2[] {
        return controlPoints;
    };

    private static bSplineToBezierInternal(controlPoints: Vector2[], degree: number): Stack<Vector2[]> {
        const stack: Stack<Vector2[]> = new Stack();
        degree = Math.min(degree, controlPoints.length - 1);
        const num = controlPoints.length - 1;
        const array = [...controlPoints];
        
        if (degree === num)
            stack.Push(array);
        else {
            for (let i = 0; i < num - degree; i++) {
                const array2: Vector2[] = [];
                array2[0] = array[i];
                for (let j = 0; j < degree - 1; j++) {
                    array2[j + 1] = array[i + 1];
                    for (let k = 1; k < degree - j; k++) {
                        const num2 = Math.min(k, num - degree - i);
                        array[i + k] = (array[i + k].scale(num2).add(array[i + k + 1])).divide(num2 + 1);
                    }
                }

                array2[degree] = array[i + 1];
                stack.Push(array2);
            }

            stack.Push(array.slice(num - degree));
        }

        return stack;
    };

    private static catmullFindPoint(vec1: Vector2, vec2: Vector2, vec3: Vector2, vec4: Vector2, t: number): Vector2 {
        const t2 = t * t;
        const t3 = t * t2;

        return new Vector2(
            0.5 * (2 * vec2.x + (-vec1.x + vec3.x) * t + (2 * vec1.x - 5 * vec2.x + 4 * vec3.x - vec4.x) * t2 + (-vec1.x + 3 * vec2.x - 3 * vec3.x + vec4.x) * t3),
            0.5 * (2 * vec2.y + (-vec1.y + vec3.y) * t + (2 * vec1.y - 5 * vec2.y + 4 * vec3.y - vec4.y) * t2 + (-vec1.y + 3 * vec2.y - 3 * vec3.y + vec4.y) * t3)
        );
    };

    private static bezierApproximate(controlPoints: Vector2[], output: Vector2[], subdivisionBuffer1: Vector2[], subdivisionBuffer2: Vector2[], count: number): void {
        const l: Vector2[] = subdivisionBuffer2;
        const r: Vector2[] = subdivisionBuffer1;

        this.bezierSubdivide(controlPoints, l, r, subdivisionBuffer1, count);

        for (let i = 0; i < count - 1; i++) {
            l[count + i] = r[i + 1];
        }

        output.push(controlPoints[0]);

        for (let i = 1; i < count - 1; i++) {
            const index = 2 * i;
            const p = (l[index - 1].add(l[index].scale(2).add(l[index + 1]))).scale(0.25);
            output.push(p);
        }
    };

    private static bezierSubdivide(controlPoints: Vector2[], l: Vector2[], r: Vector2[], subdivisionBuffer: Vector2[], count: number): void {
        const midpoints: Vector2[] = subdivisionBuffer;

        for (let i = 0; i < count; i++) {
            midpoints[i] = controlPoints[i];
        }

        for (let i = 0; i < count; i++) {
            l[i] = midpoints[0];
            r[count - i - 1] = midpoints[count - i - 1];

            for (let j = 0; j < count - i - 1; j++) {
                midpoints[j] = (midpoints[j].add((midpoints[j + 1]))).divide(2);
            }
        }
    };

    private static bezierIsFlatEnough (controlPoints: Vector2[]): boolean {
        for (let i = 1; i < controlPoints.length - 1; i++) {
            if ((controlPoints[i - 1].subtract(controlPoints[i].scale(2)).add(controlPoints[i + 1])).lengthSquared() > this.BEZIER_TOLERANCE * this.BEZIER_TOLERANCE * 4) {
                return false;
            }
        }
        return true;
    };
};

export default PathApproximator;