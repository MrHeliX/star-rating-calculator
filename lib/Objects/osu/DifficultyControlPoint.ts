import { ControlPoint, ControlPointType } from "./ControlPoint";

export class DifficultyControlPoint extends ControlPoint {
    public static readonly DEFAULT = new DifficultyControlPoint({});

    public GenerateTicks: boolean = true;
    public readonly SliderVelocity: number = 1;

    constructor(difficultyControlPoint: Partial<DifficultyControlPoint>) {
        super();
        this.Time = difficultyControlPoint.Time;
        this.GenerateTicks = difficultyControlPoint.GenerateTicks ?? true;
        this.SliderVelocity = difficultyControlPoint.SliderVelocity ?? 1;
    };

    public override GetType(): ControlPointType {
        return ControlPointType.DIFFICULTY;
    };
};
