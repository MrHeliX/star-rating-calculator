import { Vector2 } from "./Objects/Vector2";
import { Precision } from "./Precision";

export class CircularArcProperties {
    public readonly IsValid: boolean;
    public readonly ThetaStart: number = 0;
    public readonly ThetaRange: number = 0;
    public readonly Direction: number = 0;
    public readonly Radius: number = 0;
    public readonly Centre: Vector2;

    public get ThetaEnd() { return this.ThetaStart + this.ThetaRange * this.Direction; };

    constructor(_c?: Partial<CircularArcProperties>, controlPoints?: Vector2[]) {
        if (!!_c) {
            this.IsValid = true;
            this.ThetaStart = _c.ThetaStart;
            this.ThetaRange = _c.ThetaRange;
            this.Direction = _c.Direction;
            this.Radius = _c.Radius;
            this.Centre = _c.Centre;
        }

        else if (!!controlPoints) {
            const [a, b, c] = controlPoints;

            if (Precision.almostEqualsNumber(0, (b.y - a.y) * (c.x - a.x) - (b.x - a.x) * (c.y - a.y))) {
                this.IsValid = false;
                this.ThetaStart = 0;
                this.ThetaRange = 0;
                this.Direction = 0;
                this.Radius = 0;
                this.Centre = null;
                return;
            }

            const d = 2 * (a.x * (b.subtract(c)).y + b.x * (c.subtract(a)).y + c.x * (a.subtract(b)).y);
            const aSq = a.lengthSquared();
            const bSq = b.lengthSquared();
            const cSq = c.lengthSquared();

            this.Centre = new Vector2(
                aSq * (b.subtract(c)).y + bSq * (c.subtract(a)).y + cSq * (a.subtract(b)).y,
                aSq * (c.subtract(b)).x + bSq * (a.subtract(c)).x + cSq * (b.subtract(a)).x
            ).divide(d);

            const dA = a.subtract(this.Centre);
            const dC = c.subtract(this.Centre);

            this.Radius = dA.length();

            this.ThetaStart = Math.atan2(dA.y, dA.x);
            let thetaEnd = Math.atan2(dC.y, dC.x);

            while (thetaEnd < this.ThetaStart)
                thetaEnd += 2 * Math.PI;

            this.Direction = 1;
            this.ThetaRange = thetaEnd - this.ThetaStart;

            let orthoAtoC = c.subtract(a);
            orthoAtoC = new Vector2(orthoAtoC.y, -orthoAtoC.x);

            if (orthoAtoC.dot(b.subtract(a)) < 0) {
                this.Direction = -this.Direction;
                this.ThetaRange = 2 * Math.PI - this.ThetaRange;
            }

            this.IsValid = true;
        };
    };
};
