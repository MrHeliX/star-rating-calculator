import { DifficultyCalculationUtils } from "../DifficultyCalculationUtils";
import { clamp } from "../helpers";
import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";
import { NORMALISED_DIAMETER, OsuDifficultyHitObject } from "../Objects/osu/HitObjects/OsuDifficultyHitObject";
import { Spinner } from "../Objects/osu/HitObjects/Spinner";

export class SpeedEvaluator {
    private static single_spacing_threshold = NORMALISED_DIAMETER * 1.25;
    private static min_speed_bonus = 200;
    private static speed_balancing_factor = 40;
    private static distance_multiplier = 0.94;

    public static EvaluateDifficultyOf(current: DifficultyHitObject): number {
        if (current.BaseObject instanceof Spinner)
            return 0;

        const osuCurrObj = current as OsuDifficultyHitObject;
        const osuPrevObj = current.Index > 0 ? current.Previous(0) as OsuDifficultyHitObject : null;

        let strainTime = osuCurrObj.StrainTime;
        const doubletapness = 1 - osuCurrObj.GetDoubleTapness(osuCurrObj.Next(0) as OsuDifficultyHitObject);

        strainTime /= clamp((strainTime / osuCurrObj.HitWindowGreat) / 0.93, 0.92, 1);

        let speedBonus = 0;

        if (DifficultyCalculationUtils.MillisecondsToBPM(strainTime) > this.min_speed_bonus)
            speedBonus = 0.75 * Math.pow((DifficultyCalculationUtils.BPMToMilliseconds(this.min_speed_bonus) - strainTime) / this.speed_balancing_factor, 2);

        const travelDistance = osuPrevObj?.TravelDistance ?? 0;
        let distance = travelDistance + osuCurrObj.MinimumJumpDistance;

        distance = Math.min(distance, this.single_spacing_threshold);

        const distanceBonus = Math.pow(distance / this.single_spacing_threshold, 3.95) * this.distance_multiplier;
        const difficulty = (1 + speedBonus + distanceBonus) * 1000 / strainTime;
        return difficulty * doubletapness;
    };
};
