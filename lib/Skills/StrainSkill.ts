import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";
import { Skill } from "./Skill";

export abstract class StrainSkill extends Skill {
    protected DecayWeight = 0.9;
    protected SectionLength = 400;
    private currentSectionPeak: number = 0;
    private currentSectionEnd = 0;
    private strainPeaks: number[] = [];
    protected ObjectStrains: number[] = [];

    protected abstract StrainValueAt(current: DifficultyHitObject): number;

    public Process(current: DifficultyHitObject): void {
        if (current.Index === 0)
            this.currentSectionEnd = Math.ceil(current.StartTime / this.SectionLength) * this.SectionLength;

        while (current.StartTime > this.currentSectionEnd) {
            this.saveCurrentPeak();
            this.startNewSectionFrom(this.currentSectionEnd, current);
            this.currentSectionEnd += this.SectionLength;
        }

        const strain = this.StrainValueAt(current);
        this.currentSectionPeak = Math.max(strain, this.currentSectionPeak);

        this.ObjectStrains.push(strain);
    };

    public CountTopWeightedStrains(): number {
        if (this.ObjectStrains.length === 0)
            return 0;

        const consistentTopStrain = this.DifficultyValue() / 10;

        if (consistentTopStrain === 0)
            return this.ObjectStrains.length;

        return this.ObjectStrains
            .map(s => 1.1 / (1 + Math.exp(-10 * (s / consistentTopStrain - 0.88))))
            .reduce((a, b) => a + b, 0);
    };

    private saveCurrentPeak(): void {
        this.strainPeaks.push(this.currentSectionPeak);
    };

    private startNewSectionFrom(time: number, current: DifficultyHitObject): void {
        this.currentSectionPeak = this.CalculateInitialStrain(time, current);
    }

    protected abstract CalculateInitialStrain(time: number, current: DifficultyHitObject): number;

    public GetCurrentStrainPeaks(): number[] {
        return this.strainPeaks.concat(this.currentSectionPeak);
    };
};
