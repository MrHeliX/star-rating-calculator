import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";
import { OsuDifficultyHitObject } from "../Objects/osu/HitObjects/OsuDifficultyHitObject";
import { OsuHitObject } from "../Objects/osu/HitObjects/OsuHitObject";
import { Slider } from "../Objects/osu/HitObjects/Slider";
import { Spinner } from "../Objects/osu/HitObjects/Spinner";

export class FlashlightEvaluator {
    private static max_opacity_bonus = 0.4;
    private static hidden_bonus = 0.2;
    private static min_velocity = 0.5;
    private static slider_multiplier = 1.3;
    private static min_angle_multiplier = 0.2;

    public static EvaluateDifficultyOf(current: DifficultyHitObject, hidden: boolean): number {
        if (current.BaseObject instanceof Spinner)
            return 0;

        const osuCurrent = current as OsuDifficultyHitObject;
        const osuHitObject = osuCurrent.BaseObject as OsuHitObject;

        const scalingFactor = 52 / osuHitObject.Radius;
        let smallDistNerf = 1;
        let cumulativeStrainTime = 0;

        let result = 0;

        let lastObj: OsuDifficultyHitObject = osuCurrent;

        let angleRepeatCount = 0;

        for (let i = 0; i < Math.min(current.Index, 10); i++) {
            const currentObj = current.Previous(i) as OsuDifficultyHitObject;
            const currentHitObject = currentObj.BaseObject as OsuHitObject;

            if (!(currentObj.BaseObject instanceof Spinner)) {
                const jumpDistance = (osuHitObject.StackedPosition.subtract(currentHitObject.StackedEndPosition)).length();

                cumulativeStrainTime += lastObj.StrainTime;

                if (i === 0)
                    smallDistNerf = Math.min(1, jumpDistance / 75);

                const stackNerf = Math.min(1, (currentObj.LazyJumpDistance / scalingFactor) / 25);
                const opacityBonus = 1 + this.max_opacity_bonus * (1 - osuCurrent.OpacityAt(currentHitObject.StartTime, hidden));

                result += stackNerf * opacityBonus * scalingFactor * jumpDistance / cumulativeStrainTime;

                if (currentObj.Angle !== undefined && currentObj.Angle !== null && osuCurrent.Angle !== undefined && osuCurrent.Angle !== null) {
                    if (Math.abs(currentObj.Angle - osuCurrent.Angle) < 0.02)
                        angleRepeatCount += Math.max(1 - 0.1 * i, 0);
                }
            }

            lastObj = currentObj;
        }

        result = Math.pow(smallDistNerf * result, 2);

        if (hidden)
            result *= 1 + this.hidden_bonus;

        result *= this.min_angle_multiplier + (1 - this.min_angle_multiplier) / (angleRepeatCount + 1);

        let sliderBonus = 0;

        if (osuCurrent.BaseObject instanceof Slider) {
            const osuSlider = osuCurrent.BaseObject;
            const pixelTravelDistance = osuSlider.LazyTravelDistance / scalingFactor;
            sliderBonus = Math.pow(Math.max(0, pixelTravelDistance / osuCurrent.TravelTime - this.min_velocity), 0.5);
            sliderBonus *= pixelTravelDistance;

            if (osuSlider.RepeatCount > 0)
                sliderBonus /= (osuSlider.RepeatCount + 1);
        }

        result += sliderBonus * this.slider_multiplier;
        return result;
    };
};
