import { HitCircle } from "../HitCircle";

export class SliderHeadCircle extends HitCircle {
    public ClassicSliderBehaviour: boolean;

    constructor(sliderHeadCircle: Partial<SliderHeadCircle>) {
        super(sliderHeadCircle);
        this.ClassicSliderBehaviour = sliderHeadCircle.ClassicSliderBehaviour;
    };
};
