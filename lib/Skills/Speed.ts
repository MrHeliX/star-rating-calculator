import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";
import { OsuDifficultyHitObject } from "../Objects/osu/HitObjects/OsuDifficultyHitObject";
import { OsuStrainSkill } from "./OsuStrainSkill";
import { RhythmEvaluator } from "./RhythmEvaluator";
import { SpeedEvaluator } from "./SpeedEvaluator";

export class Speed extends OsuStrainSkill {
    private skillMultiplier = 1.430;
    private strainDecayBase = 0.3;
    private currentStrain: number = 0;
    private currentRhythm: number = 0;
    public override ReducedSectionCount = 5;

    constructor(mods: string[]) {
        super(mods);
    };

    private strainDecay(ms: number): number {
        return Math.pow(this.strainDecayBase, ms / 1000);
    };

    protected override CalculateInitialStrain(time: number, current: DifficultyHitObject): number {
        return (this.currentStrain * this.currentRhythm) * this.strainDecay(time - current.Previous(0).StartTime);  
    };

    protected override StrainValueAt(current: DifficultyHitObject): number {
        this.currentStrain *= this.strainDecay((current as OsuDifficultyHitObject).StrainTime);
        this.currentStrain += SpeedEvaluator.EvaluateDifficultyOf(current) * this.skillMultiplier;
        
        this.currentRhythm = RhythmEvaluator.EvaluateDifficultyOf(current);

        const totalStrain = this.currentStrain * this.currentRhythm;

        return totalStrain;
    };

    public RelevantNoteCount(): number {
        if (this.ObjectStrains.length === 0)
            return 0;

        const maxStrain = Math.max(...this.ObjectStrains);
        if (maxStrain === 0)
            return 0;

        return this.ObjectStrains
            .map(strain => 1 / (1 + Math.exp(-(strain / maxStrain * 12 - 6))))
            .reduce((a, b) => a + b, 0);
    };
};
