import { EmptyHitWindows } from "./HitObjects/EmptyHitWindows";
import { HitResult } from "./HitResult";

export class DifficultyRange {
    public readonly Result: HitResult;

    public Min = 0;
    public Average = 0;
    public Max = 0;

    constructor(result: HitResult, min: number, average: number, max: number) {
        this.Result = result;
        this.Min = min;
        this.Average = average;
        this.Max = max;
    };
};

export class HitWindows {
    private readonly base_ranges: DifficultyRange[] = [
        new DifficultyRange(HitResult.Perfect, 22.4, 19.4, 13.9),
        new DifficultyRange(HitResult.Great, 64, 49, 34),
        new DifficultyRange(HitResult.Good, 97, 82, 67),
        new DifficultyRange(HitResult.OK, 127, 112, 97),
        new DifficultyRange(HitResult.Meh, 151, 136, 121),
        new DifficultyRange(HitResult.Miss, 188, 173, 158)
    ];

    private perfect = 0;
    private great = 0;
    private good = 0;
    private ok = 0;
    private meh = 0;
    private miss = 0;

    protected LowestSuccesfulHitResult(): HitResult {
        for (let result = HitResult.Meh; result <= HitResult.Perfect; result++) {
            if (this.IsHitResultAllowed(result))
                return result;
        }

        return HitResult.None;
    };

    public GetAllAvailableWindows(): { result: HitResult, length: number }[] {
        const res = [];
        for (let result = HitResult.Meh; result <= HitResult.Perfect; result++) {
            if (this.IsHitResultAllowed(result))
                res.push({ result: result, length: this.WindowFor(result) });
        }

        return res;
    };

    public IsHitResultAllowed(result: HitResult): boolean {
        return true;
    };

    public setDifficulty(difficulty: number): void {
        for (const range of this.GetRanges()) {
            const value = GetDifficultyRange(difficulty, range.Min, range.Average, range.Max);

            switch (range.Result) {
                case HitResult.Miss:
                    this.miss = value;
                    break;
                case HitResult.Meh:
                    this.meh = value;
                    break;
                case HitResult.OK:
                    this.ok = value;
                    break;
                case HitResult.Good:
                    this.good = value;
                    break;
                case HitResult.Great:
                    this.great = value;
                    break;
                case HitResult.Perfect:
                    this.perfect = value;
                    break;
            };
        }
    };

    public ResultFor(timeOffset: number): HitResult {
        timeOffset = Math.abs(timeOffset);

        for (let result = HitResult.Perfect; result >= HitResult.Miss; result--) {
            if (this.IsHitResultAllowed(result) && timeOffset <= this.WindowFor(result))
                return result;
        }

        return HitResult.None;
    };

    public WindowFor(result: HitResult): number {
        switch (result) {
            case HitResult.Perfect:
                return this.perfect;
            case HitResult.Great:
                return this.great;
            case HitResult.Good:
                return this.good;
            case HitResult.OK:
                return this.ok;
            case HitResult.Meh:
                return this.meh;
            case HitResult.Miss:
                return this.miss;
            default:
                throw new Error(`Unknown hit result: ${result}`);
        };
    };

    public CanBeHit(timeOffset: number): boolean {
        return timeOffset <= this.WindowFor(this.LowestSuccesfulHitResult());
    };

    protected GetRanges(): DifficultyRange[] {
        return this.base_ranges;
    };
};

export const GetDifficultyRange = (difficulty: number, min: number, mid: number, max: number): number => {
    if (difficulty > 5)
        return mid + (max - mid) * GetDifficultyRangeSingle(difficulty);
    if (difficulty < 5)
        return mid + (mid - min) * GetDifficultyRangeSingle(difficulty);

    return mid;
};

export const GetDifficultyRangeSingle = (difficulty: number): number => (difficulty - 5) / 5;
