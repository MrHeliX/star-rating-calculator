import { PathType, SplineType } from "./Objects/osu/PathType";
import { Vector2 } from "./Objects/Vector2";
import { Precision } from "./Precision";
import { PathControlPoint } from "./PathControlPoint";
import { Cache } from "./Objects/Cache";
import PathApproximator from "./PathApproximator";
import { CircularArcProperties } from "./CircularArcProperties";
import { clamp } from "./helpers";

export class SliderPath {
    public Version = 0;
    private expectedDistance: number = 0;
    public get ExpectedDistance() { return this.expectedDistance; };
    public set ExpectedDistance(value: number) {
        if (value !== this.ExpectedDistance) {
            this.expectedDistance = value;
            this.invalidate();
        }
    };
    public get HasValidLength() { return Precision.DefinitelyBigger(this.Distance, 0); };

    private controlPoints: PathControlPoint[] = [];
    public get ControlPoints() { return this.controlPoints; };
    public set ControlPoints(value: PathControlPoint[]) {
        if (value.length) {
            this.controlPoints.push(...value);
            this.invalidate();
        }
    };
    private readonly calculatedPath: Vector2[] = [];
    private readonly cumulativeLength: number[] = [];
    private readonly pathCache: Cache = new Cache();
    private optimisedLength = 0;
    private calculatedLength = 0;
    private readonly segmentEnds: number[] = [];
    private segmentEndDistances: number[] = [];

    constructor(sliderPath: Partial<SliderPath>) {
        this.ExpectedDistance = sliderPath.ExpectedDistance;
        this.ControlPoints = sliderPath.ControlPoints;
    };

    public get Distance() {
        this.ensureValid();
        return this.cumulativeLength.length === 0 ? 0 : this.cumulativeLength[this.cumulativeLength.length - 1];
    };

    public get CalculatedDistance() {
        this.ensureValid();
        return this.calculatedLength;
    };

    private optimiseCatmull: boolean;
    public get OptimiseCatmull() { return this.optimiseCatmull; };
    public set OptimiseCatmull(value: boolean) {
        this.optimiseCatmull = value;
        this.invalidate();
    };

    public GetPathToProgress(path: Vector2[], p0: number, p1: number): void {
        this.ensureValid();

        const d0 = this.progressToDistance(p0);
        const d1 = this.progressToDistance(p1);

        path.length = 0;
        let i = 0;

        for (; i < this.calculatedPath.length && this.cumulativeLength[i] < d0; i++) {
            // ???? quality osu code
        }

        path.push(this.interpolateVertices(i, d0));

        for (; i < this.calculatedPath.length && this.cumulativeLength[i] <= d1; i++) {
            path.push(this.calculatedPath[i]);
        }

        path.push(this.interpolateVertices(i, d1));
    };

    public PositionAt(progress: number): Vector2 {
        this.ensureValid();

        const d = this.progressToDistance(progress);
        return this.interpolateVertices(this.indexOfDistance(d), d);
    };

    public PointsInSegment(controlPoint: PathControlPoint): PathControlPoint[] {
        let found = false;
        const pointsInCurrentSegment: PathControlPoint[] = [];

        for (const point of this.ControlPoints) {
            if (!point.Type) {
                if (!found)
                    pointsInCurrentSegment.length = 0;
                else {
                    pointsInCurrentSegment.push(point);
                    break;
                }
            }

            pointsInCurrentSegment.push(point);

            if (point.ToString() === controlPoint.ToString())
                found = true;
        }

        return pointsInCurrentSegment;
    };

    public GetSegmentEnds(): number[] {
        this.ensureValid();

        return this.segmentEndDistances.map(d => d / this.Distance);
    };

    public invalidate(): void {
        this.pathCache.Invalidate();
        this.Version++;
    };

    private ensureValid(): void {
        if (this.pathCache.IsValid)
            return;

        this.calculatePath();
        this.calculateLength();

        this.pathCache.Validate();
    };

    private calculatePath(): void {
        this.calculatedPath.length = 0;
        this.segmentEnds.length = 0;
        this.optimisedLength = 0;

        if (this.ControlPoints.length === 0)
            return;

        const vertices: Vector2[] = this.ControlPoints.map(p => p.Position);
        let start = 0;

        for (let i = 0; i < this.ControlPoints.length; i++) {
            if (!this.ControlPoints[i].Type && i < this.ControlPoints.length - 1)
                continue;

            const segmentVertices = vertices.slice(start, i + 1);
            const segmentType = this.ControlPoints[start].Type ?? PathType.LINEAR;

            if (segmentVertices.length === 1)
                this.calculatedPath.push(segmentVertices[0]);
            else if (segmentVertices.length > 1) {
                const subPath = this.calculateSubPath(segmentVertices, segmentType);
                const skipFirst = this.calculatedPath.length > 0 && subPath.length > 0 && this.calculatedPath[this.calculatedPath.length - 1].equals(subPath[0]);

                for (let j = skipFirst ? 1 : 0; j < subPath.length; j++) {
                    this.calculatedPath.push(subPath[j]);
                }
            }

            if (i > 0)
                this.segmentEnds.push(this.calculatedPath.length - 1);

            start = i;
        };
    };

    private calculateSubPath(subControlPoints: Vector2[], type: PathType): Vector2[] {
        switch (type.Type) {
            case SplineType.Linear:
                return PathApproximator.LinearToPiecewiseLinear(subControlPoints);
            case SplineType.PerfectCurve: {
                if (subControlPoints.length !== 3)
                    break;

                const circularArcProperties = new CircularArcProperties(undefined, subControlPoints);

                if (!circularArcProperties.IsValid)
                    break;

                const subPoints = (2 * circularArcProperties.Radius <= 0.1) ? 2 : Math.max(2, Math.ceil(circularArcProperties.ThetaRange / (2 * Math.acos(1 - (0.1 / circularArcProperties.Radius)))));

                if (subPoints >= 1000)
                    break;

                const subPath = PathApproximator.CircularArcToPiecewiseLinear(subControlPoints);

                if (subPath.length === 0)
                    break;

                return subPath;
            }
            case SplineType.Catmull: {
                const subPath = PathApproximator.CatmullToPiecewiseLinear(subControlPoints);

                if (!this.OptimiseCatmull)
                    return subPath;

                const optimisedPath: Vector2[] = [];
                let lastStart: Vector2 = null;
                let lengthRemovedSinceStart = 0;

                for (let i = 0; i < subPath.length; i++) {
                    if (!lastStart) {
                        optimisedPath.push(subPath[i]);
                        lastStart = subPath[i];
                        continue;
                    }

                    const distFromStart = lastStart.distance(subPath[i]);
                    lengthRemovedSinceStart += subPath[i - 1].distance(subPath[i]);

                    const catmull_detail = 50;
                    const catmull_segment_length = catmull_detail * 2;

                    if (distFromStart > 6 || (i + 1) % catmull_segment_length === 0 || i === subPath.length - 1) {
                        optimisedPath.push(subPath[i]);
                        this.optimisedLength += lengthRemovedSinceStart - distFromStart;

                        lastStart = null;
                        lengthRemovedSinceStart = 0;
                    }
                }

                return optimisedPath;
            }
        }

        return PathApproximator.BSplineToPiecewiseLinear(subControlPoints, type.Degree ?? subControlPoints.length);
    };

    private calculateLength(): void {
        this.calculatedLength = this.optimisedLength;
        this.cumulativeLength.length = 0;
        this.cumulativeLength.push(0);

        for (let i = 0; i < this.calculatedPath.length - 1; i++) {
            const diff = this.calculatedPath[i + 1].subtract(this.calculatedPath[i]);
            this.calculatedLength += diff.length();
            this.cumulativeLength.push(this.calculatedLength);
        }

        this.segmentEndDistances = [];

        for (let i = 0; i < this.segmentEnds.length; i++) {
            this.segmentEndDistances[i] = this.cumulativeLength[this.segmentEnds[i]];
        }

        if (this.calculatedLength !== this.ExpectedDistance) {
            if (this.calculatedPath.length >= 2 && this.calculatedPath[this.calculatedPath.length - 1].equals(this.calculatedPath[this.calculatedPath.length - 2]) && this.ExpectedDistance > this.calculatedLength) {
                this.cumulativeLength.push(this.calculatedLength);
                return;
            }

            this.cumulativeLength.splice(this.cumulativeLength.length - 1, 1);

            let pathEndIndex = this.calculatedPath.length - 1;

            if (this.calculatedLength > this.ExpectedDistance) {
                while (this.cumulativeLength.length > 0 && this.cumulativeLength[this.cumulativeLength.length - 1] >= this.ExpectedDistance) {
                    this.cumulativeLength.splice(this.cumulativeLength.length - 1, 1);
                    this.calculatedPath.splice(pathEndIndex--, 1);
                }
            }

            if (pathEndIndex <= 0) {
                this.cumulativeLength.push(0);
                return;
            }
            
            const dir = (this.calculatedPath[pathEndIndex].subtract(this.calculatedPath[pathEndIndex - 1])).normalize();

            this.calculatedPath[pathEndIndex] = this.calculatedPath[pathEndIndex - 1].add(dir.scale(this.ExpectedDistance - this.cumulativeLength[this.cumulativeLength.length - 1]));
            this.cumulativeLength.push(this.ExpectedDistance);
        }
    };

    public MirrorPath(): void {
        this.calculatedPath.forEach(p => {
            p.y = -p.y;
        });
    };

    private progressToDistance(progress: number): number {
        return clamp(progress, 0, 1) * this.Distance;
    };

    private interpolateVertices(i: number, d: number): Vector2 {
        if (this.calculatedPath.length === 0)
            return new Vector2(0, 0);

        if (i <= 0)
            return this.calculatedPath[0];
        if (i >= this.calculatedPath.length)
            return this.calculatedPath[this.calculatedPath.length - 1];

        const p0: Vector2 = this.calculatedPath[i - 1];
        const p1: Vector2 = this.calculatedPath[i];

        const d0: number = this.cumulativeLength[i - 1];
        const d1: number = this.cumulativeLength[i];

        if (Precision.almostEqualsNumber(d0, d1))
            return p0;

        const w: number = (d - d0) / (d1 - d0);
        return p0.add(p1.subtract(p0).scale(w));
    };

    private indexOfDistance(d: number): number {
        let i: number = this.cumulativeLength.indexOf(d);
        if (i < 0)
            i = ~this.cumulativeLength.findIndex(c => c > d);
        
        if (i < 0)
            i = ~i;

        return i;
    };
};
