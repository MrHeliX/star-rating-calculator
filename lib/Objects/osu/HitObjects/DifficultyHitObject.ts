import { HitObject } from "./HitObject";

/**
 * HitObject with additional difficulty properties required to calculate the star rating
 */
export class DifficultyHitObject {
    private readonly difficultyHitObjects: DifficultyHitObject[];
    public Index: number;
    public readonly BaseObject: HitObject;
    public readonly LastObject: HitObject;
    public readonly DeltaTime: number;
    public readonly StartTime: number;
    public readonly EndTime: number;
    
    constructor(hitObject: HitObject, lastObject: HitObject, clockRate: number, objects: DifficultyHitObject[], index: number) {
        this.difficultyHitObjects = objects;
        this.Index = index;
        this.BaseObject = hitObject;
        this.LastObject = lastObject;
        this.DeltaTime = (hitObject.StartTime - lastObject.StartTime) / clockRate;
        this.StartTime = hitObject.StartTime / clockRate;
        this.EndTime = hitObject.GetEndTime() / clockRate;
    };

    public Previous(backwardsIndex: number): DifficultyHitObject {
        const index = this.Index - (backwardsIndex + 1);
        return index >= 0 && index < this.difficultyHitObjects.length ? this.difficultyHitObjects[index] : null;
    };

    public Next(forwardsIndex: number): DifficultyHitObject {
        const index = this.Index + (forwardsIndex + 1);
        return index >= 0 && index < this.difficultyHitObjects.length ? this.difficultyHitObjects[index] : null;
    };
};