import { describe, expect, test } from "vitest";
import { calculateStarRating } from "../lib";
import { round } from "lodash";
import { StarRatingModCombination } from "../lib/Interfaces/interfaces";
const sr_data: { id: number, star_ratings: { [key: string]: number } }[] = require("./sr_data.json");

describe("Calculate star ratings", async () => {
    for (const data of sr_data) {
        test(`Correctly calculates star rating for map ${data.id}`, async () => {
            const calculated_star_rating = await calculateStarRating(data.id, {
                path: `${__dirname}/osu_files/${data.id}.osu`
            });
            const correct_star_rating = data.star_ratings.NM;
            expect(round(calculated_star_rating.star_ratings.NM?.star_rating ?? 0, 2)).to.equal(round(correct_star_rating, 2));
        });
    }

    for (const data of sr_data.slice(0, 100)) {
        test(`Correctly calculates mod star ratings for map ${data.id}`, async () => {
            const calculated_star_ratings = await calculateStarRating(data.id, {
                path: `${__dirname}/osu_files/${data.id}.osu`,
                allModCombinations: true
            });

            Object.keys(calculated_star_ratings.star_ratings).forEach(key => {
                const calc_sr = calculated_star_ratings.star_ratings[key as StarRatingModCombination];
                const correct_sr = data.star_ratings[key];
                const difference = Math.abs((calc_sr?.star_rating ?? 0) - correct_sr);
                expect(difference).toBeLessThanOrEqual(0.01);
            });
        });
    }
});
