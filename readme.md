# osu-sr-calculator
Package to calculate star ratings with any mod combination on osu! beatmaps
Using the recently updated star rating algorithm (november 2024)

## Installation
```sh
npm install osu-sr-calculator --save
```
```sh
yarn add osu-sr-calculator
```

## Usage
```typescript
import { calculateStarRating } from 'osu-sr-calculator';

const starRating = await calculateStarRating(mapId, options);
```

The calculateStarRating function accepts four parameters:
* mapId: the map id of the beatmap (make sure you use the id from the /b url)
* options (optional): an object with additional options:
  * path: the path to the .osu file. If omitted (or invalid), the beatmap file will be downloaded from osu!
  * checksum: only used if the `path` parameter is included. This checksum will be compared to the checksum of the file found and if they don't match, a new version will be downloaded from osu
  * mods: an array containing the mods you want to include in the calculation. Omitting this will default to nomod.
  * allModCombinations: if you set this to true, all unique star ratings for all possible mod combinations will be calculated. The `mods` parameter will be ignored

This function returns an object containing the calculated star ratings for the requested mod combination. In case of allCombinations, the object will contain all mod combinations with their corresponding star ratings. 

## Examples

* Nomod star rating:
```typescript
const starRating = await calculateStarRating(53);
/* Response:
{
    map_id: 1616712,
    star_ratings: {
        NM: {
            star_rating: 3.01,
            aim_rating: 1.22,
            speed_rating: 1.56,
            flashlight_rating: 0
        }
    }
}
*/
```

* HDDT star rating:
```typescript
const starRating = await calculateStarRating(53, { mods: ["HD", "DT"] });
/* Response:
{
  map_id: 53,
  star_ratings: {
    DT: {
      star_rating: 2.735860220359399,
      aim_rating: 1.523548357182464,
      speed_rating: 0.9245861222096409,
      flashlight_rating: 0
    }
  }
}
*/
```

* All possible star ratings:
```typescript
const starRating = await calculateStarRating(53, { allModCombinations: true });
/* Response:
{
  map_id: 53,
  star_ratings: {
    NM: {
      star_rating: 2.16744499506542,
      aim_rating: 1.2229963002109072,
      speed_rating: 0.6674524713948623,
      flashlight_rating: 0
    },
    DT: {
      star_rating: 2.735860220359399,
      aim_rating: 1.523548357182464,
      speed_rating: 0.9245861222096409,
      flashlight_rating: 0
    },
    HT: {
      star_rating: 1.8168007574947767,
      aim_rating: 1.0292870550736428,
      speed_rating: 0.5352279304274877,
      flashlight_rating: 0
    },
    FL: {
      star_rating: 2.2551847754071184,
      aim_rating: 1.2229963002109072,
      speed_rating: 0.6674524713948623,
      flashlight_rating: 0.2250617956885513
    },
    DTFL: {
      star_rating: 2.8484620204020183,
      aim_rating: 1.523548357182464,
      speed_rating: 0.9245861222096409,
      flashlight_rating: 0.32546995821729424
    },
    HTFL: {
      star_rating: 1.8864928491799893,
      aim_rating: 1.0292870550736428,
      speed_rating: 0.5352279304274877,
      flashlight_rating: 0.16651754693919033
    },
    HR: {
      star_rating: 2.531701319459565,
      aim_rating: 1.4434931163267344,
      speed_rating: 0.7107917744933737,
      flashlight_rating: 0
    },
    HRDT: {
      star_rating: 3.174610386554348,
      aim_rating: 1.795317574069883,
      speed_rating: 0.9717464259789171,
      flashlight_rating: 0
    },
    HRHT: {
      star_rating: 2.124093716471545,
      aim_rating: 1.2137628591775727,
      speed_rating: 0.5736284204550813,
      flashlight_rating: 0
    },
    HRFL: {
      star_rating: 2.631850542616678,
      aim_rating: 1.4434931163267344,
      speed_rating: 0.7107917744933737,
      flashlight_rating: 0.28329496193372905
    },
    HRDTFL: {
      star_rating: 3.305673303792074,
      aim_rating: 1.795317574069883,
      speed_rating: 0.9717464259789171,
      flashlight_rating: 0.40990716459244575
    },
    HRHTFL: {
      star_rating: 2.203215831905636,
      aim_rating: 1.2137628591775727,
      speed_rating: 0.5736284204550813,
      flashlight_rating: 0.20963051380803988
    },
    EZ: {
      star_rating: 1.798430035637836,
      aim_rating: 0.9910282808642945,
      speed_rating: 0.6363150364246655,
      flashlight_rating: 0
    },
    EZDT: {
      star_rating: 2.304126953415762,
      aim_rating: 1.2430277766277935,
      speed_rating: 0.8867192569440014,
      flashlight_rating: 0
    },
    EZHT: {
      star_rating: 1.5039819402206744,
      aim_rating: 0.8351364515076349,
      speed_rating: 0.5094607669651633,
      flashlight_rating: 0
    },
    EZFL: {
      star_rating: 1.8664284296115408,
      aim_rating: 0.9910282808642945,
      speed_rating: 0.6363150364246655,
      flashlight_rating: 0.16276285314792546
    },
    EZDTFL: {
      star_rating: 2.3881180649606932,
      aim_rating: 1.2430277766277935,
      speed_rating: 0.8867192569440014,
      flashlight_rating: 0.235476166689555
    },
    EZHTFL: {
      star_rating: 1.5585352962584755,
      aim_rating: 0.8351364515076349,
      speed_rating: 0.5094607669651633,
      flashlight_rating: 0.12039957822311322
    }
  }
}
*/
```

## Accuracy
This package parses the beatmap and calculates the star rating in exactly the same way as in the official osu! source code.
Due to slight difference between C# and TypeScript, a fault margin of at most 0.01 should be expected.
Accuracy on "unrankable" beatmaps can be further off.