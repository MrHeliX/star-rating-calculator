import { DifficultyCalculationUtils } from "../DifficultyCalculationUtils";
import { clamp } from "../helpers";
import { DifficultyHitObject } from "../Objects/osu/HitObjects/DifficultyHitObject";
import { MIN_DELTA_TIME, OsuDifficultyHitObject } from "../Objects/osu/HitObjects/OsuDifficultyHitObject";
import { Slider } from "../Objects/osu/HitObjects/Slider";
import { Spinner } from "../Objects/osu/HitObjects/Spinner";

const intMaxValue = 2147483647;

class Island {
    private readonly deltaDifferenceEpsilon: number;
    public Delta: number = intMaxValue;
    public DeltaCount: number = 0;

    constructor(epsilon: number, delta?: number) {
        this.deltaDifferenceEpsilon = epsilon;
        if (delta !== undefined) {
            this.Delta = Math.max(delta, MIN_DELTA_TIME);
            this.DeltaCount++;
        }
    };

    public AddDelta(delta: number): void {
        if (this.Delta === intMaxValue)
            this.Delta = Math.max(delta, MIN_DELTA_TIME);

        this.DeltaCount++;
    };

    public IsSimilarPolarity(other: Island): boolean {
        return this.DeltaCount % 2 === other.DeltaCount % 2;
    };

    public Equals(other?: Island): boolean {
        if (!other)
            return false;

        return Math.abs(this.Delta - other.Delta) < this.deltaDifferenceEpsilon && this.DeltaCount === other.DeltaCount;
    };
};

export class RhythmEvaluator {
    private static history_time_max = 5 * 1000;
    private static history_objects_max = 32;
    private static rhythm_overall_multiplier = 0.95;
    private static rhythm_ratio_multiplier = 12;

    public static EvaluateDifficultyOf(current: DifficultyHitObject): number {
        if (current.BaseObject instanceof Spinner)
            return 0;

        let rhythmComplexitySum = 0;
        const deltaDifferenceEpsilon = (current as OsuDifficultyHitObject).HitWindowGreat * 0.3;

        let island = new Island(deltaDifferenceEpsilon);
        let previousIsland = new Island(deltaDifferenceEpsilon);
        const islandCounts: { Island: Island, Count: number }[] = [];

        let startRatio = 0;
        let firstDeltaSwitch = false;
        const historicalNoteCount = Math.min(current.Index, this.history_objects_max);
        let rhythmStart = 0;

        while (rhythmStart < historicalNoteCount - 2 && current.StartTime - current.Previous(rhythmStart).StartTime < this.history_time_max) {
            rhythmStart++;
        }

        let prevObj = current.Previous(rhythmStart) as OsuDifficultyHitObject;
        let lastObj = current.Previous(rhythmStart + 1) as OsuDifficultyHitObject;

        for (let i = rhythmStart; i > 0; i--) {
            const currObj = current.Previous(i - 1) as OsuDifficultyHitObject;

            const timeDecay = (this.history_time_max - (current.StartTime - currObj.StartTime)) / this.history_time_max;
            const noteDecay = (historicalNoteCount - i) / historicalNoteCount;
            const currHistoricalDecay = Math.min(noteDecay, timeDecay);
            
            let currDelta = currObj.StrainTime;
            let prevDelta = prevObj.StrainTime;
            let lastDelta = lastObj.StrainTime;

            const deltaDifferenceRatio = Math.min(prevDelta, currDelta) / Math.max(prevDelta, currDelta);
            const currRatio = 1 + this.rhythm_ratio_multiplier * Math.min(0.5, Math.pow(Math.sin(Math.PI / deltaDifferenceRatio), 2));

            const fraction = Math.max(prevDelta / currDelta, currDelta / prevDelta);
            const fractionMultiplier = clamp(2 - fraction / 8, 0, 1);

            const windowPenalty = Math.min(1, Math.max(0, Math.abs(prevDelta - currDelta) - deltaDifferenceEpsilon) / deltaDifferenceEpsilon);
            let effectiveRatio = windowPenalty * currRatio * fractionMultiplier;

            if (firstDeltaSwitch) {
                if (Math.abs(prevDelta - currDelta) < deltaDifferenceEpsilon)
                    island.AddDelta(currDelta);
                else {
                    if (currObj.BaseObject instanceof Slider)
                        effectiveRatio *= 0.125;

                    if (prevObj.BaseObject instanceof Slider)
                        effectiveRatio *= 0.3;

                    if (island.IsSimilarPolarity(previousIsland))
                        effectiveRatio *= 0.5;

                    if (lastDelta > prevDelta + deltaDifferenceEpsilon && prevDelta > currDelta + deltaDifferenceEpsilon)
                        effectiveRatio *= 0.125;

                    if (previousIsland.DeltaCount === island.DeltaCount)
                        effectiveRatio *= 0.5;

                    const islandCount = islandCounts.find(x => x.Island.Equals(island));
                    if (!!islandCount) {
                        const countIndex = islandCounts.indexOf(islandCount);

                        if (previousIsland.Equals(island))
                            islandCount.Count++;

                        const power = DifficultyCalculationUtils.Logistic(island.Delta, 58.33, 0.24, 2.75);
                        effectiveRatio *= Math.min(3 / islandCount.Count, Math.pow(1 / islandCount.Count, power));

                        islandCounts[countIndex] = { Island: islandCount.Island, Count: islandCount.Count };
                    }
                    else
                        islandCounts.push({ Island: island, Count: 1 });

                    const doubletapness = prevObj.GetDoubleTapness(currObj);
                    effectiveRatio *= 1 - doubletapness * 0.75;

                    rhythmComplexitySum += Math.sqrt(effectiveRatio * startRatio) * currHistoricalDecay;

                    startRatio = effectiveRatio;

                    previousIsland = island;

                    if (prevDelta + deltaDifferenceEpsilon < currDelta)
                        firstDeltaSwitch = false;

                    island = new Island(deltaDifferenceEpsilon, currDelta);
                }
            }
            else if (prevDelta > currDelta + deltaDifferenceEpsilon) {
                firstDeltaSwitch = true;

                if (currObj.BaseObject instanceof Slider)
                    effectiveRatio *= 0.6;

                if (prevObj.BaseObject instanceof Slider)
                    effectiveRatio *= 0.6;

                startRatio = effectiveRatio;

                island = new Island(deltaDifferenceEpsilon, currDelta);
            }
            
            lastObj = prevObj;
            prevObj = currObj;
        }

        return Math.sqrt(4 + rhythmComplexitySum * this.rhythm_overall_multiplier) / 2;
    };
};
