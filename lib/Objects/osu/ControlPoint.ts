export enum ControlPointType {
    TIMING = "timing",
    DIFFICULTY = "difficulty"
};

export abstract class ControlPoint {
    private time = 0;
    public get Time() { return this.time; };
    public set Time(value: number) {
        if (this.time === value)
            return;
        this.time = value;
    };

    public abstract GetType(): ControlPointType;
};
